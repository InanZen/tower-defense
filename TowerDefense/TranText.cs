﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TowerDefense
{
    public class TranText
    {
        public bool Active;
        public String Text;
        public float TranTime;
        float TimeLeft;
        public Vector2 Velocity;
        public Vector2 Position;
        public float Opacity { get { return TimeLeft / TranTime; } }
        public Color Color;

        public TranText(String text, Vector2 pos, float time, Vector2 vel, Color c)
        {
            Text = text;
            Position = pos;
            Velocity = vel;
            TranTime = time;
            TimeLeft = time;
            Active = true;
            Color = c;
        }

        public void Update(float dT)
        {
            if (Active)
            {
                TimeLeft -= dT;
                if (TimeLeft <= 0)
                    Active = false;
                else
                {
                    Position = new Vector2(Position.X + Velocity.X * dT, Position.Y + Velocity.Y * dT);
                }
            }
        }
    }
}
