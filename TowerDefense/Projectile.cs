﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
namespace TowerDefense
{
    public class Projectile
    {
        public bool Active;
        public Vector2 TilePos;
        public Vector2 Position { get { return game.ScreenPosition(TilePos); } }
        public Npc Target;
        public ProjectileSpecs Specs;
        public float Rotation;

        float damageMod;
        float velocityMod;
        GameComponent game;

        public float Damage { get { return Specs.Damage + damageMod; } }
        public float Velocity { get { return Specs.Velocity + velocityMod; } }
        public ProjectileType Type { get { return Specs.Type; } }
        public ushort gID { get { return Specs.gID; } }

        public Projectile(ProjectileSpecs specs, Vector2 tilePos, Npc target, float dmgMod, float velMod)
        {
            Specs = specs;
            TilePos = tilePos;
            Target = target;
            game = target.game;
            damageMod = dmgMod;
            velocityMod = velMod;
            Active = true;

        }
        public void Update(float dT)
        {
            if (Active)
            {
                    Vector2 diff = Target.TilePosition - TilePos;
                    diff.Normalize(); // direction unit vector
                    TilePos = new Vector2(TilePos.X + diff.X * Velocity * dT, TilePos.Y + diff.Y * Velocity * dT);   // position +  direction * velocity * dt
                    Vector2 diffAfter = Target.TilePosition - TilePos;
                    if (Math.Sign(diff.X) != Math.Sign(diffAfter.X) || Math.Sign(diff.Y) != Math.Sign(diffAfter.Y)) // if projectile has reached target
                    {
                        Active = false;
                        TilePos = Target.TilePosition;
                    }
                    Rotation = (float)(Math.PI / 2 + Math.Atan2(Target.Position.Y - Position.Y, Target.Position.X - Position.X)); // face target
                
            }
        }
    }
    public enum ProjectileType
    {
        Bullet = 0,
        Missile = 1,
        Mortar = 2
    }
    public struct ProjectileSpecs
    {
        /// <summary>
        /// Global ID number of this Specification
        /// </summary>
        public int ID;
        /// <summary>
        /// Type of Projectile
        /// </summary>
        public ProjectileType Type;
        /// <summary>
        /// Global ID of projectile sprite
        /// </summary>
        public ushort gID;
        /// <summary>
        /// Base velocity value of Projectile
        /// </summary>
        public float Velocity;
        /// <summary>
        /// Base damage value of Projectile
        /// </summary>
        public float Damage;

        public float RangeMod;

        public float DamageRadius;
        public string HitSound;
        public string FireSound;
        public int Cost;

        /// <summary>
        /// 0 - no buff, 1 - slow, 2 - weak, 3 - dmg over time
        /// </summary>
        public byte Buff;
        public float BuffMod;
        public float BuffTime;
        public float BuffNTime;


        public static ProjectileSpecs Null { get { return new ProjectileSpecs { ID = -1 }; } }
        public static ProjectileSpecs[] AllAvailable = new ProjectileSpecs[] 
        {
            // ################## BULLETS ########################
            new ProjectileSpecs { // small bullet
                ID = 0, 
                Type = ProjectileType.Bullet, 
                Cost = 50, 
                gID = 104, 
                Velocity = 8, 
                Damage = 2, 
                FireSound = "gun2", 
                HitSound = "hit_light"
            }, 
            new ProjectileSpecs { // normal bullet
                ID = 1, 
                Type = ProjectileType.Bullet,
                Cost = 75, 
                gID = 106, 
                Velocity = 8, 
                Damage = 2.5f, 
                FireSound = "gun",
                HitSound = "hit_light",
                RangeMod = 0.1f 
            }, 
            new ProjectileSpecs { // gold bullet
                ID = 2, 
                Type = ProjectileType.Bullet, 
                Cost = 150, 
                gID = 107, 
                Velocity = 9,
                Damage = 3,
                FireSound = "gun2", 
                HitSound = "hit_light",
                RangeMod = 0.2f
            }, 
            new ProjectileSpecs { // fire bullet
                ID = 3, 
                Type = ProjectileType.Bullet, 
                Cost = 175, gID = 108, 
                Velocity = 9,
                Damage = 3, 
                FireSound = "gun2", 
                HitSound = "hit_light",
                RangeMod = 0.3f 
            },
            new ProjectileSpecs { //giant bullet
                ID = 4, 
                Type = ProjectileType.Bullet, 
                Cost = 250,
                gID = 109, 
                Velocity = 7,
                Damage = 4, 
                FireSound = "gun", 
                HitSound = "hit_light",
                RangeMod = -0.2f 
            }, 
            // ################## MISSILES ########################
            new ProjectileSpecs {  // normal missile
                ID = 5,
                Type = ProjectileType.Missile,
                Cost = 50, 
                gID = 112, 
                Velocity = 4,
                Damage = 3,
                DamageRadius = 0.2f,
                FireSound = "airgun", 
                HitSound = "explosion"
            },
            new ProjectileSpecs { // gold missile
                ID = 6,
                Type = ProjectileType.Missile,
                gID = 113, 
                Cost = 75, 
                Velocity = 4.5f, 
                Damage = 3.5f, 
                DamageRadius = 0.2f,
                FireSound = "airgun",
                HitSound = "explosion"
            }, 
            new ProjectileSpecs { // big missile
                ID = 7,
                Type = ProjectileType.Missile,
                Cost = 150,
                gID = 114,
                Velocity = 3f,
                Damage = 5,
                DamageRadius = 0.5f,
                RangeMod = 0.5f,
                FireSound = "airgun",
                HitSound = "explosion"
            }, 
            new ProjectileSpecs { // fast missile
                ID = 8, 
                Type = ProjectileType.Missile, 
                Cost = 200, 
                gID = 115, 
                Velocity = 5f, 
                Damage = 4, 
                FireSound = "airgun",
                HitSound = "explosion",
                DamageRadius = 0.3f, 
                RangeMod = 0.7f 
            }, 
            // ################ MORTARS ###############
            new ProjectileSpecs { // slowing mortar
                ID = 9, 
                Type = ProjectileType.Mortar, 
                Cost = 50, 
                gID = 97,
                Velocity = 3f, 
                Damage = 0, 
                DamageRadius = 0.75f,
                FireSound = "spit", 
                HitSound = "fire", 
                Buff = 1, 
                BuffMod = 0.75f,
                BuffTime = 3.5f
            },
            new ProjectileSpecs { // weakening mortar
                ID = 10, 
                Type = ProjectileType.Mortar, 
                Cost = 100, 
                gID = 96,
                Velocity = 3f,
                Damage = 0, 
                DamageRadius = 0.6f,
                FireSound = "spit",
                HitSound = "fire",
                Buff = 2, 
                BuffMod = 1.2f, 
                BuffTime = 2.5f 
            },
            new ProjectileSpecs { // poison mortar
                ID = 11, 
                Type = ProjectileType.Mortar, 
                Cost = 100, 
                gID = 98,
                Velocity = 3f,
                Damage = 0, 
                DamageRadius = 0.7f,
                FireSound = "spit",
                HitSound = "fire",
                Buff = 3, 
                BuffMod = 0.5f,
                BuffTime = 3,
                BuffNTime = 0.2f 
            } 
        };
    }

}
