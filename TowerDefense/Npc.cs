﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TowerDefense
{
    public class Npc
    {
        public GameComponent game;
        ushort tile;
        int x;
        int y;
        public ushort Tile
        {
            get { return tile; }
            set
            {
                tile = value;
                x = tile % game.lvlWidth;
                y = tile / game.lvlWidth;
            }
        }
        public int X
        {
            get { return x; }
            set
            {
                x = value;
                tile = (ushort)(x + y * game.lvlWidth);
            }
        }
        public int Y
        {
            get { return y; }
            set
            {
                y = value;
                tile = (ushort)(x + y * game.lvlWidth);
            }
        }

        public bool Active;
        public Vector2 Position;
        public Vector2 TilePosition { get { return game.GameWorldPosition(Position); } }
        public int[] Path;
        public int PathIdx;
        public ushort gID { get { return Type.gID; } }
        public float Velocity;
        public float HP;
        public float InitHP { get { return Type.HP + lvl * Type.HPinc; } } 
        public float TransAmt;
        public int lvl;
        public NpcType Type;

        public List<Buff> Buffs = new List<Buff>();


        public Npc(NpcType type, int wave, GameComponent game)
        {
            this.game = game;
            this.Type = type;
            this.lvl = wave;
            this.HP = InitHP;
            this.Velocity = type.Velocity;
            this.Tile = game.startTile;
            this.Active = true;
        }

        public void Update(float dT)
        {
            if (Active)
            {
                float vMod = 1f;
                // apply all speed debuffs
                for (int i = 0; i < Buffs.Count; i++)
                    if (Buffs[i].Type == 1)
                        vMod *= Buffs[i].Mod;

                TransAmt += dT * vMod;
                if (TransAmt >= 1 / Velocity)
                {
                    TransAmt -= 1 / Velocity;
                    PathIdx++;
                    Tile = (ushort)Path[PathIdx];
                    Position = game.PositionAtTile(tile);
                    if (tile == game.endTile) // npc has reached the player's base
                    {
                        Active = false;
                        game.BaseHP -= 1;
                        game.animatedText.Add(new TranText("-1 HP", Position, 1.2f, new Vector2(0, -25), Color.Red));
                        Main.Audio.PlaySound("scream");
                    }
                }
                else
                {
                    var start = game.PositionAtTile(Path[PathIdx]);
                    var end = game.PositionAtTile(Path[PathIdx + 1]);
                    Position = Vector2.Lerp(start, end, (Velocity) * TransAmt);
                }
                // update buff durations, remove expired
                for (int i = Buffs.Count - 1; i >= 0; i--)
                {
                    Buff buff = Buffs[i];
                    buff.Duration -= dT;
                    if (buff.Type == 3) // dmg over time
                    {
                        buff.CurTime -= dT;
                        if (buff.CurTime <= 0)
                        {
                            buff.CurTime += buff.NextTime;
                            HP -= buff.Mod; // do buff dmg
                            if (HP <= 0) // npc dies
                            {
                                Active = false;
                                game.Money += Type.MoneyDrop;
                            }
                        }
                    }
                    if (buff.Duration <= 0)
                        Buffs.RemoveAt(i);
                }
            }
        }
    }

    public struct NpcType
    {
        public int ID;
        public float HP;
        public float HPinc;
        public float Velocity;
        public int MoneyDrop;
        public ushort gID;
        public static NpcType[] AllAvailable = new NpcType[] 
        {
            new NpcType() { ID = 0, HP = 10, HPinc = 1, Velocity = 1f,MoneyDrop = 10, gID = 65 },
            new NpcType() { ID = 1, HP = 8, HPinc = 0.7f, Velocity = 2f, MoneyDrop = 12, gID = 64 },
            new NpcType() { ID = 2, HP = 9, HPinc = 0.8f, Velocity = 1.5f, MoneyDrop = 11, gID = 66 },
            new NpcType() { ID = 3, HP = 12, HPinc = 1.5f, Velocity = 0.8f, MoneyDrop = 15, gID = 67 }
        };
        public static NpcType Random
        {
            get
            {
                int id = GameComponent.random.Next(0, AllAvailable.Length);
                return AllAvailable[id];
            }
        }
    }

    
    public class Buff
    {
        /// <summary>
        /// Buff type
        /// 0 - no buff, 1 - slow, 2 - weak, 3 - dmg
        /// </summary>
        public byte Type;
        public float Duration;
        public float Mod;
        public float CurTime;
        public float NextTime;
    }
}
