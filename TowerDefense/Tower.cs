﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TowerDefense
{
    public class Tower
    {
        GameComponent game;
        ushort tile;
        int x;
        int y;

        public ushort Tile
        {
            get { return tile; }
            set
            {
                tile = value;
                x = tile % game.lvlWidth;
                y = tile / game.lvlWidth;
            }
        }
        public int X
        {
            get { return x; }
            set
            {
                x = value;
                tile = (ushort)(x + y * game.lvlWidth);
            }
        }
        public int Y
        {
            get { return y; }
            set
            {
                y = value;
                tile = (ushort)(x + y * game.lvlWidth);
            }
        }

        public Vector2 Position { get { return game.PositionAtTile(tile); } }
        public Vector2 TilePosition { get { return new Vector2(x + 0.5f, y + 0.5f); } }

        public ushort PlatformGID { get { return platform.gID; } }
        public ushort TurretGID
        {
            get
            {
                if (turret == null)
                    return 0; 
                return turret.Specs.gID;
            }
        }
        public float Range { 
            get 
            {
                if (turret == null)
                    return 0;
                return turret.Range + platform.RangeMod + turret.ProjSpecs.RangeMod;
            } 
        }
        public float FireRate {
            get 
            {
                if (turret == null)
                    return -1;
                return turret.FireRate + platform.FireRateMod;
            } 
        }

        public ProjectileSpecs ProjSpecs { get { return turret.ProjSpecs; } }

        public float Rotation { get { return turret.Rotation; } set { turret.Rotation = value; } }

        /// <summary>
        /// Platform total Cost + Turret total Cost + Projectile Cost
        /// </summary>
        public int Cost
        {
            get
            {
                int c = platform.Cost;
                if (turret != null)
                    c += turret.Cost + turret.ProjSpecs.Cost;
                return c;
            }
        }

        float NextShot;
        Npc target;

        public TowerPlatform platform;
        public Turret turret;

        public Tower(TowerPlatform tPlatform, Turret tTurret, GameComponent game)
        {
            this.game = game;
            this.turret = tTurret;
            this.platform = tPlatform;
        }

        public void Update(float dT)
        {
            if (turret == null)
                return;
            if (NextShot != 0)
            {
                NextShot -= dT;
                if (NextShot < 0)
                    NextShot = 0;
            }
            Vector2 pos = new Vector2((Position.X - game.paddingX) / game.tileW, (Position.Y - game.paddingY - game.ControlBar.Position.Height) / game.tileH);
            if (target != null)
            {

                Vector2 npcpos = new Vector2((target.Position.X - game.paddingX) / game.tileW, (target.Position.Y - game.paddingY - game.ControlBar.Position.Height) / game.tileH);
                if (!target.Active || Vector2.Distance(pos, npcpos) >= Range) // current target inactive or out of range
                    target = null;                
                else
                {
                    Rotation = (float)(Math.PI / 2 + Math.Atan2(target.Position.Y - Position.Y, target.Position.X - Position.X)); // face target
                    if (turret.ProjSpecs.ID != -1 && NextShot == 0) // fire at target
                    {
                        Projectile projectile = new Projectile(ProjSpecs, TilePosition, target, turret.DamageMod, turret.VelocityMod);
                        game.projectileList.Add(projectile);
                        NextShot = FireRate;                           
                        Main.Audio.PlaySound(projectile.Specs.FireSound);
                    }
                }
            }
            if (target == null) // check for new target
            {
                float distance = float.MaxValue;
                Npc selected = null;
                foreach (Npc npc in game.npcList)
                {
                    if (npc != null)
                    {
                        Vector2 npcpos = new Vector2((npc.Position.X - game.paddingX) / game.tileW, (npc.Position.Y - game.paddingY - game.ControlBar.Position.Height) / game.tileH);
                        float d = Vector2.Distance(pos, npcpos);
                        if (d < distance)
                        {
                            distance = d;
                            selected = npc;
                        }
                    }
                }
                if (distance <= Range)
                    target = selected;
            }

        }
    }

    public class TowerPlatform
    {
        public float FireRateMod;
        public float RangeMod;
        public int RangeUpgradeIndx;
        public int FireRateUpgradeIndx;
        public TowerPlatformSpecs Specs;

        public ushort gID { get { return Specs.gID; } }
        /// <summary>
        /// Platform base cost + purchased upgrades cost
        /// </summary>
        public int Cost
        {
            get
            {
                int c = Specs.Cost; // base spec cost
                for (int i = 0; i < FireRateUpgradeIndx; i++) // add all purchased fire rate upgrades cost
                    c += Specs.FireRateUpgrades[i].Item1;
                for (int i = 0; i < RangeUpgradeIndx; i++) // add all purchased range upgrades cost
                    c += Specs.RangeUpgrades[i].Item1;
                return c;
            }
        }

        public TowerPlatform(TowerPlatformSpecs specs)
        {
            Specs = specs;
        }
    }
    public struct TowerPlatformSpecs
    {
        /// <summary>
        /// Global ID number of this Specification
        /// </summary>
        public int ID;
        /// <summary>
        /// global ID of sprite
        /// </summary>
        public ushort gID;
        /// <summary>
        /// initial cost
        /// </summary>
        public int Cost;
        /// <summary>
        /// Costs and number of fire rate upgrades
        /// </summary>
        public Tuple<int, float>[] FireRateUpgrades;
        /// <summary>
        /// Costs and number of range upgrades
        /// </summary>
        public Tuple<int, float>[] RangeUpgrades;

        public static TowerPlatformSpecs[] AllAvailable = new TowerPlatformSpecs[] {
            new TowerPlatformSpecs() { 
                ID = 0,
                gID = 35, 
                Cost = 0, 
                FireRateUpgrades = new Tuple<int,float>[0], 
                RangeUpgrades = new Tuple<int,float>[0] 
            },
            new TowerPlatformSpecs() { 
                ID = 1,
                gID = 36, 
                Cost = 20, 
                FireRateUpgrades = new Tuple<int,float> []
                {
                    new Tuple<int,float>(50, -0.1f),
                    new Tuple<int,float>(50, -0.1f)
                },
                RangeUpgrades = new Tuple<int,float> []
                {
                    new Tuple<int,float>(50, 0.3f),
                    new Tuple<int,float>(50, 0.3f)
                }
            },
            new TowerPlatformSpecs() { 
                ID = 2,
                gID = 37, 
                Cost = 20, 
                FireRateUpgrades = new Tuple<int,float> []
                {
                    new Tuple<int,float>(75, -0.15f),
                    new Tuple<int,float>(75, -0.15f),
                    new Tuple<int,float>(75, -0.15f)
                },
                RangeUpgrades = new Tuple<int,float> []
                {
                    new Tuple<int,float>(50, 0.3f)
                }
            },
            new TowerPlatformSpecs() { 
                ID = 3,
                gID = 38, 
                Cost = 20, 
                FireRateUpgrades = new Tuple<int,float> []
                {
                    new Tuple<int,float>(50, -0.1f)
                },
                RangeUpgrades = new Tuple<int,float> []
                {
                    new Tuple<int,float>(75, 0.4f),
                    new Tuple<int,float>(75, 0.4f),
                    new Tuple<int,float>(75, 0.4f)
                }
            }
        };
    }



    public class Turret
    {
        /// <summary>
        /// Type of the Turret
        /// </summary>
        public TurretType Type { get { return Specs.Type; } }
        /// <summary>
        /// Base specs of the Tower
        /// </summary>
        public TurretSpecs Specs;
        /// <summary>
        /// Supported Projectile type
        /// </summary>        
        public ProjectileType SupportedProj { get { return Specs.SupportedProj; } }
        public ProjectileSpecs ProjSpecs = ProjectileSpecs.Null;
        
        // Upgradeables
        public float DamageMod;
        public float VelocityMod;
        public float RangeMod;
        public float FireRateMod;

        public int DamageUpgradeIndx;
        public int VelocityUpgradeIndx;
        public int RangeUpgradeIndx;
        public int FireRateUpgradeIndx;

        /// <summary>
        /// Turret specs range + turret range mod
        /// </summary>
        public float Range { get { return Specs.Range + RangeMod; } }
        /// <summary>
        /// Turret specs fire rate + turret fire rate mod
        /// </summary>
        public float FireRate { get { return Specs.FireRate + FireRateMod; } }

        /// <summary>
        /// Turret spec cost + purchased upgrades costs
        /// </summary>
        public int Cost
        {
            get
            {
                int c = Specs.Cost; // base spec cost
                for (int i = 0; i < FireRateUpgradeIndx; i++) // add all purchased fire rate upgrades cost
                    c += Specs.FireRateUpgrades[i].Item1;
                for (int i = 0; i < RangeUpgradeIndx; i++) // add all purchased range upgrades cost
                    c += Specs.RangeUpgrades[i].Item1;
                for (int i = 0; i < DamageUpgradeIndx; i++) // add all purchased damage upgrades cost
                    c += Specs.DamageUpgrades[i].Item1;
                for (int i = 0; i < VelocityUpgradeIndx; i++) // add all purchased velocity upgrades cost
                    c += Specs.VelocityUpgrades[i].Item1;
                return c;
            }
        }

        public float Rotation;
        public Turret(TurretSpecs specs)
        {
            Specs = specs;
        }
    }
    public enum TurretType
    {
        Shooter = 0,
        Launcher = 1,
        Mortar = 2
    }
    public struct TurretSpecs
    {
        /// <summary>
        /// Global ID number of this Specification
        /// </summary>
        public int ID;
        /// <summary>
        /// Type of the Turret
        /// </summary>
        public TurretType Type;
        /// <summary>
        /// global ID of sprite
        /// </summary>
        public ushort gID;
        /// <summary>
        /// Base range value of Tower
        /// </summary>
        public float Range;
        /// <summary>
        /// Costs and value of range upgrades
        /// </summary>
        public Tuple<int, float>[] RangeUpgrades;
        /// <summary>
        /// Base fire rate value of Tower
        /// </summary>
        public float FireRate;
        /// <summary>
        /// Costs and number of fire rate upgrades
        /// </summary>
        public Tuple<int, float>[] FireRateUpgrades;


        /// <summary>
        /// Costs and number of velocity modifier upgrades
        /// </summary>
        public Tuple<int, float>[] VelocityUpgrades;
        /// <summary>
        /// Costs and number of damage modifier upgrades
        /// </summary>
        public Tuple<int, float>[] DamageUpgrades;

        /// <summary>
        /// initial cost
        /// </summary>
        public int Cost;
        /// <summary>
        /// Supported Projectile type
        /// </summary>
        public ProjectileType SupportedProj;

        public static TurretSpecs[] AllAvailable = new TurretSpecs[] {      
            new TurretSpecs { 
                ID = 0,
                Type = TurretType.Shooter,  
                Cost = 50, 
                gID = 80, 
                FireRate = 0.6f, 
                Range = 2.5f, 
                SupportedProj = ProjectileType.Bullet, 
                RangeUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(15, 0.2f), 
                    new Tuple<int,float>(30, 0.2f), 
                    new Tuple<int,float>(40, 0.2f) 
                },
                FireRateUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(25, -0.03f), 
                    new Tuple<int,float>(35, -0.03f)
                },
                DamageUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(25, 0.5f)
                },
                VelocityUpgrades = new Tuple<int,float>[] { 
                }
            },           
            new TurretSpecs { 
                ID = 1,
                Type = TurretType.Shooter,  
                Cost = 75, 
                gID = 81, 
                FireRate = 0.6f, 
                Range = 3f, 
                SupportedProj = ProjectileType.Bullet, 
                RangeUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(50, 0.25f),
                    new Tuple<int,float>(50, 0.25f),
                    new Tuple<int,float>(50, 0.25f),
                    new Tuple<int,float>(50, 0.25f)
                },
                FireRateUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(50, 0.5f)
                },
                DamageUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(50, 0.5f)
                },
                VelocityUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(50, 0.5f)
                }
            },        
            new TurretSpecs { 
                ID = 2,
                Type = TurretType.Shooter,  
                Cost = 100, 
                gID = 82, 
                FireRate = 0.55f, 
                Range = 2.8f, 
                SupportedProj = ProjectileType.Bullet, 
                RangeUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(20, 0.25f), 
                    new Tuple<int,float>(40, 0.25f), 
                    new Tuple<int,float>(60, 0.25f) 
                },
                FireRateUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(30, -0.03f), 
                    new Tuple<int,float>(60, -0.04f), 
                    new Tuple<int,float>(90, -0.05f)
                },
                DamageUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(40, 0.5f), 
                    new Tuple<int,float>(80, 0.5f)
                },
                VelocityUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(50, 1f)
                }
            },                     
            new TurretSpecs { 
                ID = 3,
                Type = TurretType.Launcher,  
                Cost = 85, 
                gID = 84, 
                FireRate = 1.2f, 
                Range = 3f, 
                SupportedProj = ProjectileType.Missile, 
                RangeUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(50, 0.4f), 
                    new Tuple<int,float>(50, 0.4f)
                },
                FireRateUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(50, -0.1f), 
                    new Tuple<int,float>(50, -0.1f)
                },
                DamageUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(50, 1f) 
                },
                VelocityUpgrades = new Tuple<int,float>[] {}
            },                   
            new TurretSpecs { 
                ID = 4,
                Type = TurretType.Launcher,  
                Cost = 120, 
                gID = 85, 
                FireRate = 1f, 
                Range = 3.5f, 
                SupportedProj = ProjectileType.Missile, 
                RangeUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(35, 0.3f), 
                    new Tuple<int,float>(35, 0.3f), 
                    new Tuple<int,float>(35, 0.3f) 
                },                
                FireRateUpgrades = new Tuple<int,float>[] {},
                DamageUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(30, 0.5f), 
                    new Tuple<int,float>(30, 0.5f)
                },
                VelocityUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(50, 0.4f), 
                    new Tuple<int,float>(50, 0.4f)
                }
            },                   
            new TurretSpecs { 
                ID = 5,
                Type = TurretType.Launcher,  
                Cost = 120, 
                gID = 86, 
                FireRate = 1f, 
                Range = 3f, 
                SupportedProj = ProjectileType.Missile,                 
                RangeUpgrades = new Tuple<int,float>[] {},
                FireRateUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(50, -0.1f), 
                    new Tuple<int,float>(75, -0.1f), 
                    new Tuple<int,float>(100, -0.1f)
                },
                DamageUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(50, 1f), 
                    new Tuple<int,float>(75, 1f), 
                    new Tuple<int,float>(100, 1f) 
                },
                VelocityUpgrades = new Tuple<int,float>[] {}
            },                           
            new TurretSpecs { 
                ID = 6,
                Type = TurretType.Mortar,  
                Cost = 50, 
                gID = 88, 
                FireRate = 3f, 
                Range = 3f, 
                SupportedProj = ProjectileType.Mortar, 
                RangeUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(20, 0.4f), 
                    new Tuple<int,float>(30, 0.4f),
                    new Tuple<int,float>(40, 0.4f)
                },
                FireRateUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(30, -0.2f)
                },
                DamageUpgrades = new Tuple<int,float>[] { 
                },
                VelocityUpgrades = new Tuple<int,float>[] { 
                }
            },                       
            new TurretSpecs { 
                ID = 7,
                Type = TurretType.Mortar,  
                Cost = 75, 
                gID = 89, 
                FireRate = 2.5f, 
                Range = 2.5f, 
                SupportedProj = ProjectileType.Mortar, 
                RangeUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(20, 0.5f), 
                    new Tuple<int,float>(30, 0.5f)
                },
                FireRateUpgrades = new Tuple<int,float>[] { 
                    new Tuple<int,float>(30, -0.2f),
                    new Tuple<int,float>(60, -0.2f)
                },
                DamageUpgrades = new Tuple<int,float>[] { 
                },
                VelocityUpgrades = new Tuple<int,float>[] { 
                }
            }
        };

    }
}
