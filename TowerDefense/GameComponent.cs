using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNA_GUI_Controls;


namespace TowerDefense
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class GameComponent : Microsoft.Xna.Framework.DrawableGameComponent
    {
        public GameComponent(Game game)
            : base(game)
        {
            Main main = game as Main;
            GUI = main.GUI;
            GUI.OnClick += OnControlClick;
        }

        // Tileset gIDs
        UInt16[] sceneryGIDs = new ushort[] { 40, 41, 42, 43, 44, 45, 46, 47, 48, 49 };
        UInt16 startGID = 32;
        UInt16 endGID = 33;
        UInt16 rangeGID = 56;
        UInt16 hpBarGID = 57;
        UInt16 markerGID = 58;
        UInt16 sellGID = 59;
        UInt16 buyGID = 60;
        UInt16 upgradeGID = 61;
       //       pathDirectionGIDtable                { 0, 1, 2,  3, 4,  5,  6,  7, 8,  9, 10, 11, 12, 13, 14, 15 }
        ushort[] pathDirectionGIDtable = new ushort[] { 0, 0, 0, 21, 0, 24, 23, 30, 0, 25, 26, 28, 22, 27, 29, 31 };
        //       groundDirectionGIDtable                { 0, 1, 2,  3, 4,  5,  6,  7, 8,  9, 10, 11, 12, 13, 14, 15 }
        ushort[] groundDirectionGIDtable = new ushort[] { 0, 6, 2, 10, 4, 14, 12, 18, 8, 15, 13, 17, 11, 16, 19, 20 };

        SpriteBatch spriteBatch;

        Texture2D background;
        SpriteSheet TileSet;
        public UInt16[] ground;
        public UInt16[] road;
        public UInt16[] scenery;
        public UInt16 startTile;
        public UInt16 endTile;
        public BitArray path;
        Vector2 HalfOrigin;


        Texture2D level;
        public int tileW, tileH;
        public int paddingX, paddingY;
        public int lvlWidth, lvlHeight;
        public List<Tower> towerList = new List<Tower>();
        public List<Npc> npcList = new List<Npc>();
        public List<Projectile> projectileList = new List<Projectile>();
        public List<AnimatedSprite> animatedList = new List<AnimatedSprite>();
        public List<TranText> animatedText = new List<TranText>();
        SpriteSheet ExplosionSheet;

        KeyboardState oldKeyState;
        MouseState oldMouseState;
        int mouseTile;
        Tower selectedTower;
        int selectedTurret = -1;
        int selectedProj = -1;
        bool placingTower;

        public static Random random = new Random();

        Controller GUI;
        public FrameBox PlatformBar;
        public FrameBox TurretBar;
        public FrameBox ProjectileBar;
        public FrameBox StatusBar;
        public FrameBox ControlBar;
        public FrameBox WaveBar;
        int SideBarWidth;
        int TopBarHeight;

        TextArea clockText;
        Texture2D crosshair;

        bool ended;
        int waveCount = 20;
        int currentWave = 0;
        float nextWave = 1;
        float waveFreq = 20;
        List<Npc> nextNpcs = new List<Npc>();
        List<Npc> waveNpcs = new List<Npc>();
        float speedMod = 0f;
        float difficultyMod = 0f;
        int numOfWaves = 20;
        int baseHP;
        public int BaseHP
        {
            get { return baseHP; }
            set { 
                baseHP = value; 
                UpdateStatusBar();
                if (baseHP == 0)
                    EndGame();
            }
        }
        int money;
        public int Money
        {
            get { return money; }
            set
            {
                money = value;
                UpdateStatusBar();
                int cost = 0;
                var controls = GUI.FindChildren("cost");
                foreach (Control c in controls)
                {
                    TextArea text = c as TextArea;
                    if (text != null && int.TryParse(text.Text, out cost))
                    {
                        if (money < cost)
                            text.Color = Color.DarkRed;
                        else
                            text.Color = Color.Goldenrod;
                    }
                }
                if (placingTower)
                {
                    TextArea txt = PlatformBar.FindChild("cost_val", false) as TextArea;
                    if (txt != null)
                    {
                        if (money < selectedTower.platform.Cost)
                            txt.Color = Color.DarkRed;
                        else
                            txt.Color = Color.Goldenrod;
                    }
                    txt = TurretBar.FindChild("cost_val", false) as TextArea;
                    if (txt != null)
                    {
                        if (money < selectedTower.platform.Cost + selectedTower.turret.Cost)
                            txt.Color = Color.DarkRed;
                        else
                            txt.Color = Color.Goldenrod;
                    } 
                    txt = ProjectileBar.FindChild("cost_val", false) as TextArea;
                    if (txt != null)
                    {
                        if (money < selectedTower.Cost)
                            txt.Color = Color.DarkRed;
                        else
                            txt.Color = Color.Goldenrod;
                    }
                }
            }
        }

        int GameAreaWidth { get { return GraphicsDevice.Viewport.Width - SideBarWidth; } }
        int GameAreaHeight { get { return GraphicsDevice.Viewport.Height - TopBarHeight; } }


        void Window_ClientSizeChanged(object sender, EventArgs e)
        {
            GUI.ClearChildren();
            SetUpGUI();

            tileW = GameAreaWidth / lvlWidth;
            tileH = GameAreaHeight / lvlHeight;
            if (Main.Settings.PreserveRatio)
            {
                if (tileH < tileW)
                    tileW = tileH;
                else
                    tileH = tileW;
            }
            paddingX = (GameAreaWidth - tileW * lvlWidth) / 2;
            paddingY = (GameAreaHeight - tileH * lvlHeight) / 2;

            UpdateWaveBar();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        public override void Initialize()
        {
            Game.Window.ClientSizeChanged += Window_ClientSizeChanged;
            GUI.ClearChildren();
            Main.Audio.Status = 1;
            base.Initialize();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                GUI.OnClick -= OnControlClick;
                Game.Window.ClientSizeChanged -= Window_ClientSizeChanged;
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            TileSet = new SpriteSheet(Game.Content.Load<Texture2D>("tileset"), 16, 16, 0);
            ExplosionSheet = new SpriteSheet(Game.Content.Load<Texture2D>("explosion"), 64, 64, 0);
            ExplosionSheet.Padding = 2;
            HalfOrigin = new Vector2(TileSet.TileW / 2f, TileSet.TileH / 2f);
           // level = Game.Content.Load<Texture2D>(@"levels/Level 1");
            oldKeyState = Keyboard.GetState();
            oldMouseState = Mouse.GetState();
            crosshair = Game.Content.Load<Texture2D>(@"gui/crosshair6");
            background = Game.Content.Load<Texture2D>("background");

            SetUpGUI();
        }
        public void LoadLevel(LevelData data)
        {
            money = data.Money;
            baseHP = data.HP;
            waveFreq = data.WaveTime;
            waveCount = data.WaveCount;
            try
            {
                using (FileStream fs = new FileStream(Path.Combine(Main.AppDir, "levels", data.ImgPath), FileMode.Open))
                {
                    level = Texture2D.FromStream(GraphicsDevice, fs);
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            UpdateStatusBar();
            SetUpLevel();
        }
        void EndGame(bool win = false)
        {
            ended = true;
            TogglePause();
            int w2 = GraphicsDevice.Viewport.Width / 2;
            int h2 = GraphicsDevice.Viewport.Height  / 2;
            FrameBox frame = GUI.AddFrame("endframe", new Rectangle(w2 - 200, h2 - 200, 300, 250), (int)GUI_Frame_Style.Default);
            frame.BorderH = 6;
            frame.BorderW = 6;
            Rectangle inner = frame.InnerRect;
            if (win)
            {
                TextArea text = GUI.AddTextArea("text", "Congratulations, you have defended the base!", new Rectangle(inner.X, inner.Y, inner.Width, inner.Height / 2), frame);
                text.Color = Color.Green;
                Main.Audio.PlaySound("win");
            }
            else
            {
                TextArea text = GUI.AddTextArea("text", "Game Over", new Rectangle(inner.X, inner.Y, inner.Width, inner.Height / 2), frame);
                text.Color = Color.DarkRed;
                Main.Audio.PlaySound("destroyed");
            }
            GUI.AddButton("exit", "Exit to main", new Rectangle(inner.Center.X - 100, inner.Y + (int)(inner.Height * 0.75f), 200, 50), OnButtonClick, (int)GUI_Button_Style.Default, frame);
            
        }
        void SetUpGUI()
        {
            SideBarWidth = 250;
            TopBarHeight = 100;
            int sidebarW4 = (SideBarWidth) / 4;

            // ################# STATUS BAR ############################
            StatusBar = GUI.AddFrame("statusbar", new Rectangle(GraphicsDevice.Viewport.Width - SideBarWidth, 0, SideBarWidth, 128), (int)GUI_Frame_Style.Default);
            StatusBar.BorderW = 6;
            StatusBar.BorderH = 6;
            ShowStatusBar();

            int sidebarElementH = (int)((GraphicsDevice.Viewport.Height - StatusBar.Position.Height) / 3);

            // ################# PLATFORM BAR ############################
            PlatformBar = GUI.AddFrame("platformbar", new Rectangle(StatusBar.Position.X, StatusBar.Position.Bottom - StatusBar.BorderH, SideBarWidth, sidebarElementH + 6), (int)GUI_Frame_Style.Default);
            PlatformBar.BorderW = 6;
            PlatformBar.BorderH = 6;

            // ################# TURRET BAR ############################
            TurretBar = GUI.AddFrame("turretbar", new Rectangle(PlatformBar.Position.X, PlatformBar.Position.Bottom - PlatformBar.BorderH, SideBarWidth, sidebarElementH + 6), (int)GUI_Frame_Style.Default);
            TurretBar.BorderW = 6;
            TurretBar.BorderH = 6;

            // ################# PROJECTILE BAR ############################
            ProjectileBar = GUI.AddFrame("projectilebar", new Rectangle(TurretBar.Position.X, TurretBar.Position.Bottom - TurretBar.BorderH, SideBarWidth, sidebarElementH + 6), (int)GUI_Frame_Style.Default);
            ProjectileBar.BorderW = 6;
            ProjectileBar.BorderH = 6;            

            if (selectedTower != null)
                UpdateTowerInfo();
            else
                ShowPlatformChoice();



            // #################### Control bar #########################
            int controlBarH = 100;
            int controlBarW = 300;
            ControlBar = GUI.AddFrame("controlBar", new Rectangle(0, 0, controlBarW, controlBarH), (int)GUI_Frame_Style.Default);
            int barPadding = 8;
            int barH2 = controlBarH / 2 - barPadding;
            int x = barPadding;
            GUI.AddButton("pause", null, new Rectangle(x, barPadding, 64, barH2), OnButtonClick, (int)GUI_Button_Style.Pause);
            x += barPadding + 64;
            GUI.AddButton("resume", null, new Rectangle(x, barPadding, 64, barH2), OnButtonClick, (int)GUI_Button_Style.Play);
            x += barPadding + 64;
            var slider = GUI.AddSlider("speedSlider", new Rectangle(x, barPadding, controlBarW - x - barPadding, barH2), (int)GUI_Background_Style.Slider, (int)GUI_Button_Style.Slider, ControlBar);
            if (speedMod != 0)
                slider.Value = (speedMod - 1) / 11;
            slider.ValueChanged += OnSpeedChange;            
            x = barPadding;
            var txt = GUI.AddTextArea("pausetxt", "Pause", new Rectangle(x, barH2, 64, barH2), ControlBar);
            txt.Font = Main.fontSmall;
            if (speedMod == 0)
            {
                txt.Color = Color.Red;
                txt.Text = "Paused";
            }
            x += barPadding + 64;
            txt = GUI.AddTextArea("resumetxt", "Resume", new Rectangle(x, barH2, 64, barH2), ControlBar);
            txt.Font = Main.fontSmall;

            x += barPadding + 64;
            txt = GUI.AddTextArea("speedtxt", String.Format("Speed: {0}x", speedMod), new Rectangle(x, barH2, controlBarW - x, barH2), ControlBar);
            txt.Font = Main.fontSmall;

            // ####################### Wave Bar ################################################
            int waveBarW = GraphicsDevice.Viewport.Width - controlBarW - SideBarWidth;
            WaveBar = GUI.AddFrame("controlBar", new Rectangle(controlBarW, 0, waveBarW, controlBarH), 1);
            WaveBar.BorderH = 6;
            WaveBar.BorderW = 6;
            Rectangle inner = WaveBar.InnerRect;
            int barH3 = inner.Height / 4;
            int stringX = (int)Main.font.MeasureString("Next wave in:").X;
            x = inner.X + barPadding;
            int curY = inner.Y + barPadding;

            GUI.AddTextArea("nextwavetext", "Next wave in:", new Rectangle(x, curY, stringX, barH3), WaveBar);
            x += stringX + barPadding;
            clockText = GUI.AddTextArea("clock", string.Format("{0}s", Math.Round(nextWave, 1)), new Rectangle(x, curY, 100, barH3), WaveBar);
            clockText.Alignment = TextAlignment.Left;
            clockText.Color = Color.DarkBlue;

            x = inner.X + barPadding;
            curY += barH3;
            var text = GUI.AddTextArea("nextwavetext", "Next:", new Rectangle(x, curY + 2, 48, barH3), WaveBar);
            text.Alignment = TextAlignment.Right;
            text.Font = Main.fontSmall;
            text.Color = Color.Gray;
            text = GUI.AddTextArea("nextwavetext", "Now:", new Rectangle(x, curY + barH3 + 2, 48, barH3), WaveBar);
            text.Alignment = TextAlignment.Right;
            text.Font = Main.fontSmall;
            text.Color = Color.Gray;

            x += 48 + barPadding;
            GUI.AddChild(new Control("nextwave", new Rectangle(x, barH3, inner.Right - x, barH3)));
            curY += barH3;
            GUI.AddChild(new Control("thiswave", new Rectangle(x, barH3 * 2, inner.Right - x, barH3)));
        }
        void ShowPlatformChoice()
        {
            PlatformBar.ClearChildren();

            Rectangle innerRect = PlatformBar.InnerRect;

            TextArea text = GUI.AddTextArea("select_text", "Select platform type", new Rectangle(innerRect.X, innerRect.Y, innerRect.Width, 16), PlatformBar);
            text.Font = Main.fontSmall;
            int perRow = 4;
            int elementPadding = 8;
            int wDiff = innerRect.Width / perRow;
            int elemetW = wDiff - elementPadding;
            int curY = innerRect.Y;
            for (int i = 0; i < TowerPlatformSpecs.AllAvailable.Length; i++)
            {
                int xID = i % perRow;
                int yID = i / perRow;

                int curX = innerRect.X + wDiff * xID + elementPadding / 2;
                curY = innerRect.Y + 16 + wDiff * yID + elementPadding / 2;
                if (selectedTower != null && selectedTower.platform.Specs.ID == i)
                {
                    var marker = GUI.AddSprite("marker", new Rectangle(curX, curY, elemetW, elemetW), TileSet.Texture, PlatformBar);
                    marker.Source = TileSet.GetSourceRect(markerGID);
                }
                Sprite baseSprite = GUI.AddSprite("platform", new Rectangle(curX, curY, elemetW, elemetW), TileSet.Texture, PlatformBar);
                baseSprite.Source = TileSet.GetSourceRect(TowerPlatformSpecs.AllAvailable[i].gID);
                baseSprite.Tag = i;
                baseSprite.OnClick += OnControlClick;
            }

            if (selectedTower != null && selectedTower.platform != null)
            {
                int w2 = innerRect.Width / 2;
                TowerPlatformSpecs spec = selectedTower.platform.Specs;
                curY += elemetW + elementPadding;
                text = GUI.AddTextArea("cost_text", "Cost:", new Rectangle(innerRect.X, curY, w2, 12), PlatformBar);
                text.Font = Main.fontSmall;
                text.Alignment = TextAlignment.Right;
                text = GUI.AddTextArea("cost_val", String.Format("{0}", spec.Cost), new Rectangle(innerRect.X + w2 + elementPadding, curY, w2, 12), PlatformBar);
                text.Font = Main.fontNumbers;
                text.Alignment = TextAlignment.Left;
                text.Color = Color.Goldenrod;
                if (money < spec.Cost)
                    text.Color = Color.DarkRed;
                curY += 12 + elementPadding;

                if (spec.FireRateUpgrades.Length != 0)
                {
                    text = GUI.AddTextArea("fr_text", "Fire rate:", new Rectangle(innerRect.X, curY, w2, 12), PlatformBar);
                    text.Font = Main.fontSmall;
                    text.Alignment = TextAlignment.Right;
                    text = GUI.AddTextArea("fr_up_val", String.Format("{0}x", spec.FireRateUpgrades.Length), new Rectangle(innerRect.X + w2 + elementPadding, curY, w2, 12), PlatformBar);
                    text.Font = Main.fontNumbers;
                    text.Alignment = TextAlignment.Left;
                    text.Color = Color.Gray;
                    var sprite = GUI.AddSprite("up_ico", new Rectangle(text.Position.X + (int)Main.fontNumbers.MeasureString(text.Text).X, curY, 12, 12), TileSet.Texture, PlatformBar);
                    sprite.Source = TileSet.GetSourceRect(upgradeGID);
                    curY += 12 + elementPadding;
                }

                if (spec.RangeUpgrades.Length != 0)
                {
                    text = GUI.AddTextArea("range_text", "Range:", new Rectangle(innerRect.X, curY, w2, 12), PlatformBar);
                    text.Font = Main.fontSmall;
                    text.Alignment = TextAlignment.Right;
                    text = GUI.AddTextArea("range_up_val", String.Format("{0}x", spec.RangeUpgrades.Length), new Rectangle(innerRect.X + w2 + elementPadding, curY, w2, 12), PlatformBar);
                    text.Font = Main.fontNumbers;
                    text.Alignment = TextAlignment.Left;
                    text.Color = Color.Gray;
                    var sprite = GUI.AddSprite("up_ico", new Rectangle(text.Position.X + (int)Main.fontNumbers.MeasureString(text.Text).X, curY, 12, 12), TileSet.Texture, PlatformBar);
                    sprite.Source = TileSet.GetSourceRect(upgradeGID);
                }
            }
        }
        void ShowTurretChoice()
        {
            TurretBar.ClearChildren();

            if (selectedTower == null || selectedTower.platform == null)
                return;

            var innerRect = TurretBar.InnerRect;

            TextArea text = GUI.AddTextArea("turretbar_select_text", "Select turret type", new Rectangle(innerRect.X, innerRect.Y, innerRect.Width, 16), TurretBar);
            text.Font = Main.fontSmall;

            int perRow = 4;
            int elementPadding = 8;
            int wDiff = innerRect.Width / perRow;

            int maxW = (innerRect.Width / perRow) - elementPadding / 2;
            int elementW = Math.Min(64, maxW);
            int elementH = Math.Min(64, innerRect.Height / 6);

            int curY = 0;
            int turretID = selectedTurret;
            if (placingTower && selectedTower.turret != null)
                turretID = selectedTower.turret.Specs.ID;
            
            for (int i = 0; i < TurretSpecs.AllAvailable.Length; i++)
            {
                int xID = i % perRow;
                int yID = i / perRow;

                int curX = innerRect.X + elementW * xID + elementPadding;
                curY = innerRect.Y + 16 + elementH * yID + elementPadding;

                if (turretID == i) // mark the current selection
                {
                    var marker = GUI.AddSprite("marker", new Rectangle(curX, curY, elementW, elementH), TileSet.Texture, TurretBar);
                    marker.Source = TileSet.GetSourceRect(markerGID);
                }
                Sprite sprite = GUI.AddSprite("turret", new Rectangle(curX, curY, elementW, elementH), TileSet.Texture, TurretBar);
                sprite.Source = TileSet.GetSourceRect(TurretSpecs.AllAvailable[i].gID);
                sprite.Tag = i;
                sprite.OnClick += OnControlClick;
            }
            curY += elementH + elementPadding;

            if (turretID != -1) // display the selected spec's info
            {
                int w2 = innerRect.Width / 2;
                TurretSpecs spec = TurretSpecs.AllAvailable[turretID];
                text = GUI.AddTextArea("cost_text", "Cost:", new Rectangle(innerRect.X, curY, w2, 12), TurretBar);
                text.Font = Main.fontSmall;
                text.Alignment = TextAlignment.Right;
                text = GUI.AddTextArea("cost_val", String.Format("{0}", spec.Cost), new Rectangle(innerRect.X + w2 + elementPadding, curY, w2, 12), TurretBar);
                text.Font = Main.fontNumbers;
                text.Alignment = TextAlignment.Left;
                text.Color = Color.Goldenrod;
                if (!placingTower) // display buy button if buying for already placed tower
                {
                    text.Name = "cost"; // rename to keep color updated based on money
                    if (spec.Cost > money)
                        text.Color = Color.DarkRed;
                    var buy = GUI.AddSprite("buyturret", new Rectangle(text.Position.X + (int)Main.fontNumbers.MeasureString(text.Text).X + elementPadding, curY, 12, 12), TileSet.Texture, TurretBar);
                    buy.Source = TileSet.GetSourceRect(buyGID);
                    buy.OnClick += OnControlClick;
                    buy.OnMouseOver += ControlHighlight;
                    buy.OnMouseLeave += ControlEndHighlight;
                }
                else if (money < selectedTower.platform.Cost + selectedTower.turret.Cost)
                    text.Color = Color.DarkRed;
                curY += 12 + elementPadding;

                text = GUI.AddTextArea("fr_text", "Fire rate:", new Rectangle(innerRect.X, curY, w2, 12), TurretBar);
                text.Font = Main.fontSmall;
                text.Alignment = TextAlignment.Right;
                text = GUI.AddTextArea("fr_val", String.Format("{0}", spec.FireRate), new Rectangle(innerRect.X + w2 + elementPadding, curY, w2, 12), TurretBar);
                text.Font = Main.fontNumbers;
                text.Alignment = TextAlignment.Left;
                text.Color = Color.DarkBlue;
                text = GUI.AddTextArea("fr_up_val", String.Format(" + {0}x", spec.FireRateUpgrades.Length), new Rectangle(text.Position.X + (int)Main.fontNumbers.MeasureString(text.Text).X + elementPadding, curY, w2, 12), TurretBar);
                text.Font = Main.fontNumbers;
                text.Alignment = TextAlignment.Left;
                text.Color = Color.Gray;
                var sprite = GUI.AddSprite("up_ico", new Rectangle(text.Position.X + (int)Main.fontNumbers.MeasureString(text.Text).X, curY, 12, 12), TileSet.Texture, TurretBar);
                sprite.Source = TileSet.GetSourceRect(upgradeGID);
                curY += 12 + elementPadding;

                text = GUI.AddTextArea("range_text", "Range:", new Rectangle(innerRect.X, curY, w2, 12), TurretBar);
                text.Font = Main.fontSmall;
                text.Alignment = TextAlignment.Right;
                text = GUI.AddTextArea("range_val", String.Format("{0}", spec.Range), new Rectangle(innerRect.X + w2 + elementPadding, curY, w2, 12), TurretBar);
                text.Font = Main.fontNumbers;
                text.Alignment = TextAlignment.Left;
                text.Color = Color.DarkBlue;
                text = GUI.AddTextArea("range_up_val", String.Format(" + {0}x", spec.RangeUpgrades.Length), new Rectangle(text.Position.X + (int)Main.fontNumbers.MeasureString(text.Text).X + elementPadding, curY, w2, 12), TurretBar);
                text.Font = Main.fontNumbers;
                text.Alignment = TextAlignment.Left;
                text.Color = Color.Gray;
                sprite = GUI.AddSprite("up_ico", new Rectangle(text.Position.X + (int)Main.fontNumbers.MeasureString(text.Text).X, curY, 12, 12), TileSet.Texture, TurretBar);
                sprite.Source = TileSet.GetSourceRect(upgradeGID);
                curY += 12 + elementPadding;

                if (spec.DamageUpgrades.Length != 0)
                {
                    text = GUI.AddTextArea("dmg_text", "Damage:", new Rectangle(innerRect.X, curY, w2, 12), TurretBar);
                    text.Font = Main.fontSmall;
                    text.Alignment = TextAlignment.Right;
                    text = GUI.AddTextArea("dmg_up_val", String.Format("{0}x", spec.DamageUpgrades.Length), new Rectangle(innerRect.X + w2 + elementPadding, curY, w2, 12), TurretBar);
                    text.Font = Main.fontNumbers;
                    text.Alignment = TextAlignment.Left;
                    text.Color = Color.Gray;
                    sprite = GUI.AddSprite("up_ico", new Rectangle(text.Position.X + (int)Main.fontNumbers.MeasureString(text.Text).X, curY, 12, 12), TileSet.Texture, TurretBar);
                    sprite.Source = TileSet.GetSourceRect(upgradeGID);
                    curY += 12 + elementPadding;
                }
                if (spec.VelocityUpgrades.Length != 0)
                {
                    text = GUI.AddTextArea("vel_text", "Velocity:", new Rectangle(innerRect.X, curY, w2, 12), TurretBar);
                    text.Font = Main.fontSmall;
                    text.Alignment = TextAlignment.Right;
                    text = GUI.AddTextArea("vel_up_val", String.Format("{0}x", spec.VelocityUpgrades.Length), new Rectangle(innerRect.X + w2 + elementPadding, curY, w2, 12), TurretBar);
                    text.Font = Main.fontNumbers;
                    text.Alignment = TextAlignment.Left;
                    text.Color = Color.Gray;
                    sprite = GUI.AddSprite("up_ico", new Rectangle(text.Position.X + (int)Main.fontNumbers.MeasureString(text.Text).X, curY, 12, 12), TileSet.Texture, TurretBar);
                    sprite.Source = TileSet.GetSourceRect(upgradeGID);
                }
            }
        }
        void ShowProjectileChoice()
        {
            ProjectileBar.ClearChildren();
            if (selectedTower == null || selectedTower.turret == null)
                return;

            var innerRect = ProjectileBar.InnerRect;

            TextArea text = GUI.AddTextArea("select_text", "Select projectile class", new Rectangle(innerRect.X, innerRect.Y, innerRect.Width, 16), ProjectileBar);
            text.Font = Main.fontSmall;
            
            ProjectileSpecs[] matching = ProjectileSpecs.AllAvailable.Where(p=>p.Type == selectedTower.turret.SupportedProj).ToArray();
            int perRow = matching.Length;
            int elementPadding = 8;
            int maxW = (innerRect.Width / perRow) - elementPadding / 2;
            int elementW = Math.Min(64, maxW);
            int elementH = Math.Min(64, innerRect.Height / 4);
            int curY = innerRect.Y + 16 + elementPadding;

            int projID = selectedProj;
            if (placingTower)
                projID = selectedTower.ProjSpecs.ID;

            for (int i = 0; i < matching.Length; i++)
            {
                int curX = innerRect.X + elementW * i + elementPadding;
                if (projID == matching[i].ID) // highlight selected                    
                {
                    var marker = GUI.AddSprite("marker", new Rectangle(curX, curY, elementW, elementH), TileSet.Texture, ProjectileBar);
                    marker.Source = TileSet.GetSourceRect(markerGID);
                }
                Sprite sprite = GUI.AddSprite("projectile", new Rectangle(curX, curY, elementW, elementH), TileSet.Texture, ProjectileBar);
                sprite.Source = TileSet.GetSourceRect(matching[i].gID);
                sprite.Tag = matching[i].ID;
                sprite.OnClick += OnControlClick;
            }
            if (projID != -1)
            {
                ProjectileSpecs spec = ProjectileSpecs.AllAvailable[projID];
                int w2 = innerRect.Width / 2;
                curY += elementH + elementPadding;
                text = GUI.AddTextArea("cost_text", "Cost:", new Rectangle(innerRect.X, curY, w2, 12), ProjectileBar);
                text.Font = Main.fontSmall;
                text.Alignment = TextAlignment.Right;
                text = GUI.AddTextArea("cost_val", String.Format("{0}", spec.Cost), new Rectangle(innerRect.X + w2 + elementPadding, curY, w2, 12), ProjectileBar);
                text.Font = Main.fontNumbers;
                text.Alignment = TextAlignment.Left;
                text.Color = Color.Goldenrod;
                if (!placingTower) // display buy button if buying for already placed tower
                {
                    text.Name = "cost"; // rename to keep color updated based on money
                    if (spec.Cost > money)
                        text.Color = Color.DarkRed;
                    var buy = GUI.AddSprite("buyproj", new Rectangle(text.Position.X + (int)Main.fontNumbers.MeasureString(text.Text).X + elementPadding, curY, 12, 12), TileSet.Texture, ProjectileBar);
                    buy.Source = TileSet.GetSourceRect(buyGID);
                    buy.OnClick += OnControlClick;
                    buy.OnMouseOver += ControlHighlight;
                    buy.OnMouseLeave += ControlEndHighlight;
                }
                else if (money < selectedTower.Cost)
                    text.Color = Color.DarkRed;
                curY += 12 + elementPadding;

                text = GUI.AddTextArea("damage_text", "Damage:", new Rectangle(innerRect.X, curY, w2, 12), ProjectileBar);
                text.Font = Main.fontSmall;
                text.Alignment = TextAlignment.Right;
                text = GUI.AddTextArea("damage_val", String.Format("{0}", spec.Damage), new Rectangle(innerRect.X + w2 + elementPadding, curY, w2, 12), ProjectileBar);
                text.Font = Main.fontNumbers;
                text.Alignment = TextAlignment.Left;
                text.Color = Color.DarkGreen;
                if (spec.DamageRadius != 0)
                {
                    text = GUI.AddTextArea("radius_val", String.Format("{0} radius", spec.DamageRadius), new Rectangle(text.Position.X + (int)Main.fontNumbers.MeasureString(text.Text).X+4, curY, w2, 12), ProjectileBar);
                    text.Font = Main.fontNumbers;
                    text.Alignment = TextAlignment.Left;
                    text.Color = Color.DarkGray;
                }
                curY += 12 + elementPadding;

                text = GUI.AddTextArea("velocity_text", "Velocity:", new Rectangle(innerRect.X, curY, w2, 12), ProjectileBar);
                text.Font = Main.fontSmall;
                text.Alignment = TextAlignment.Right;
                text = GUI.AddTextArea("velocity_val", String.Format("{0}", spec.Velocity), new Rectangle(innerRect.X + w2 + elementPadding, curY, w2, 12), ProjectileBar);
                text.Font = Main.fontNumbers;
                text.Alignment = TextAlignment.Left;
                text.Color = Color.DarkGreen;
                curY += 12 + elementPadding;

                if (spec.RangeMod != 0)
                {
                    text = GUI.AddTextArea("range_text", "Range:", new Rectangle(innerRect.X, curY, w2, 12), ProjectileBar);
                    text.Font = Main.fontSmall;
                    text.Alignment = TextAlignment.Right;
                    text = GUI.AddTextArea("range_val", String.Format("+{0}", spec.RangeMod), new Rectangle(innerRect.X + w2 + elementPadding, curY, w2, 12), ProjectileBar);
                    text.Font = Main.fontNumbers;
                    text.Alignment = TextAlignment.Left;
                    text.Color = Color.DarkBlue;
                    if (spec.RangeMod < 0) // negative range mod
                    {
                        text.Text = String.Format("{0}", spec.RangeMod);
                        text.Color = Color.DarkRed;
                    }
                    curY += 12 + elementPadding;
                }

                if (spec.Buff != 0)
                {
                    string effect = "Slow:";
                    if (spec.Buff == 2)
                        effect = "Weak:";
                    else if (spec.Buff == 3)
                        effect = "Dmg over time";
                    text = GUI.AddTextArea("buff_text", effect, new Rectangle(innerRect.X, curY, w2, 12), ProjectileBar);
                    text.Font = Main.fontSmall;
                    text.Alignment = TextAlignment.Right;
                    text = GUI.AddTextArea("buff_val", String.Format("{0}", spec.BuffMod), new Rectangle(innerRect.X + w2 + elementPadding, curY, w2, 12), ProjectileBar);
                    text.Font = Main.fontNumbers;
                    text.Alignment = TextAlignment.Left;
                    text.Color = Color.DarkGreen;
                    text = GUI.AddTextArea("buff_val", String.Format("{0}s", spec.BuffTime), new Rectangle(text.Position.X + (int)Main.fontNumbers.MeasureString(text.Text).X + 8, curY, w2, 12), ProjectileBar);
                    text.Font = Main.fontNumbers;
                    text.Alignment = TextAlignment.Left;
                    if (spec.BuffNTime != 0)
                    {
                        text = GUI.AddTextArea("buff_val", String.Format("{0}s", spec.BuffNTime), new Rectangle(text.Position.X + (int)Main.fontNumbers.MeasureString(text.Text).X + 8, curY, w2, 12), ProjectileBar);
                        text.Font = Main.fontNumbers;
                        text.Alignment = TextAlignment.Left;
                    }
                    curY += 12 + elementPadding;
                }
            }
        }

        void ShowPlatformUpgrades()
        {
            if (selectedTower == null || selectedTower.platform == null)
                return;

            TowerPlatform platform = selectedTower.platform;

            PlatformBar.ClearChildren();

            Rectangle innerRect = PlatformBar.InnerRect;
            int w2 = innerRect.Width / 2;

            // platform ico
            int elementPadding = 8;
            int curY = innerRect.Y + elementPadding;
            Sprite sprite = GUI.AddSprite("platformico", new Rectangle(innerRect.X + elementPadding, curY, 32, 32), TileSet.Texture, PlatformBar);
            sprite.Source = TileSet.GetSourceRect(platform.gID);
            // sell text
            int txtX = (int)Main.fontSmall.MeasureString("Sell tower:").X;
            TextArea text = GUI.AddTextArea("sell_text", "Sell tower:", new Rectangle(innerRect.Right - 16 - elementPadding - 32 - txtX, curY + 10, txtX, 12), PlatformBar);
            text.Font = Main.fontSmall;
            text = GUI.AddTextArea("sell_val", String.Format("{0}", selectedTower.Cost / 2f), new Rectangle(innerRect.Right - 16 - elementPadding - 32, curY + 10, 32, 12), PlatformBar);
            text.Font = Main.fontNumbers;
            text.Color = Color.Green;
            // sell button
            sprite = GUI.AddSprite("sell", new Rectangle(innerRect.Right - 16 - elementPadding, curY + 8, 16, 16), TileSet.Texture, PlatformBar);
            sprite.Source = TileSet.GetSourceRect(sellGID);
            sprite.OnClick += OnControlClick;
            sprite.OnMouseOver += ControlHighlight;
            sprite.OnMouseLeave += ControlEndHighlight;
            sprite.Tag = 1;
            curY += 32 + elementPadding;

            if (platform.FireRateMod != 0 || platform.Specs.FireRateUpgrades.Length != 0)
            {
                text = GUI.AddTextArea("fr_text", "Fire rate:", new Rectangle(innerRect.X, curY, w2, 12), PlatformBar);
                text.Font = Main.fontSmall;
                text.Alignment = TextAlignment.Right;
                text = GUI.AddTextArea("fr_val", String.Format("{0}", platform.FireRateMod), new Rectangle(innerRect.Center.X + elementPadding, curY, w2, 12), PlatformBar);
                text.Font = Main.fontNumbers;
                text.Alignment = TextAlignment.Left;
                text.Color = Color.DarkBlue;
                curY += 14;
                if (platform.FireRateUpgradeIndx < platform.Specs.FireRateUpgrades.Length)
                {
                    //upgrade text & icon
                    string upcounttxt;
                    if (platform.FireRateUpgradeIndx == 0)
                        upcounttxt = "1st";
                    else if (platform.FireRateUpgradeIndx == 1)
                        upcounttxt = "2nd";
                    else if (platform.FireRateUpgradeIndx == 2)
                        upcounttxt = "3rd";
                    else
                        upcounttxt = String.Format("{0}th", platform.FireRateUpgradeIndx + 1);
                    text = GUI.AddTextArea("up_text", upcounttxt, new Rectangle(innerRect.X, curY, w2 - 14, 12), PlatformBar);
                    text.Font = Main.fontSmall;
                    text.Alignment = TextAlignment.Right;
                    text.Color = Color.Gray;
                    sprite = GUI.AddSprite("ico", new Rectangle(innerRect.Center.X - 12, curY, 12, 12), TileSet.Texture, PlatformBar);
                    sprite.Source = TileSet.GetSourceRect(upgradeGID);

                    var up = platform.Specs.FireRateUpgrades[platform.FireRateUpgradeIndx];
                    // modifier
                    text = GUI.AddTextArea("up_val", String.Format("{0}", up.Item2), new Rectangle(innerRect.Center.X + elementPadding, curY, w2, 12), PlatformBar);
                    text.Font = Main.fontNumbers;
                    text.Color = Color.Green;
                    text.Alignment = TextAlignment.Left;

                    // price
                    text = GUI.AddTextArea("cost", String.Format("{0}", up.Item1), new Rectangle(innerRect.Right - 12 - elementPadding - 32, curY, 32, 12), PlatformBar);
                    text.Font = Main.fontNumbers;
                    if (money < up.Item1)
                        text.Color = Color.DarkRed;
                    else
                        text.Color = Color.Goldenrod;
                    // buy button
                    sprite = GUI.AddSprite("upgradePlatform", new Rectangle(innerRect.Right - 12 - elementPadding, curY, 12, 12), TileSet.Texture, PlatformBar);
                    sprite.Source = TileSet.GetSourceRect(buyGID);
                    sprite.OnMouseOver += ControlHighlight;
                    sprite.OnMouseLeave += ControlEndHighlight;
                    sprite.OnClick += OnControlClick;
                    sprite.Tag = 0;
                    curY += 12;
                }
                curY += elementPadding;
            }

            if (platform.RangeMod != 0 || platform.Specs.RangeUpgrades.Length != 0)
            {
                text = GUI.AddTextArea("range_text", "Range:", new Rectangle(innerRect.X, curY, w2, 12), PlatformBar);
                text.Font = Main.fontSmall;
                text.Alignment = TextAlignment.Right;
                text = GUI.AddTextArea("range_val", String.Format("{0}", platform.RangeMod), new Rectangle(innerRect.Center.X + elementPadding, curY, w2, 12), PlatformBar);
                text.Font = Main.fontNumbers;
                text.Alignment = TextAlignment.Left;
                text.Color = Color.DarkBlue;
                curY += 14;
                if (platform.RangeUpgradeIndx < platform.Specs.RangeUpgrades.Length)
                {
                    //upgrade text & icon
                    string upcounttxt;
                    if (platform.RangeUpgradeIndx == 0)
                        upcounttxt = "1st";
                    else if (platform.RangeUpgradeIndx == 1)
                        upcounttxt = "2nd";
                    else if (platform.RangeUpgradeIndx == 2)
                        upcounttxt = "3rd";
                    else
                        upcounttxt = String.Format("{0}th", platform.RangeUpgradeIndx + 1);
                    text = GUI.AddTextArea("up_text", upcounttxt, new Rectangle(innerRect.X, curY, w2 - 14, 12), PlatformBar);
                    text.Font = Main.fontSmall;
                    text.Alignment = TextAlignment.Right;
                    text.Color = Color.Gray;
                    sprite = GUI.AddSprite("ico", new Rectangle(innerRect.Center.X - 12, curY, 12, 12), TileSet.Texture, PlatformBar);
                    sprite.Source = TileSet.GetSourceRect(upgradeGID);

                    var up = platform.Specs.RangeUpgrades[platform.RangeUpgradeIndx];
                    // modifier
                    text = GUI.AddTextArea("up_val", String.Format("+{0}", up.Item2), new Rectangle(innerRect.Center.X + elementPadding, curY, w2, 12), PlatformBar);
                    text.Font = Main.fontNumbers;
                    text.Color = Color.Green;
                    text.Alignment = TextAlignment.Left;

                    // price
                    text = GUI.AddTextArea("cost", String.Format("{0}", up.Item1), new Rectangle(innerRect.Right - 12 - elementPadding - 32, curY, 32, 12), PlatformBar);
                    text.Font = Main.fontNumbers;
                    if (money < up.Item1)
                        text.Color = Color.DarkRed;
                    else
                        text.Color = Color.Goldenrod;
                    // buy button
                    sprite = GUI.AddSprite("upgradePlatform", new Rectangle(innerRect.Right - 12 - elementPadding, curY, 12, 12), TileSet.Texture, PlatformBar);
                    sprite.Source = TileSet.GetSourceRect(buyGID);
                    sprite.OnMouseOver += ControlHighlight;
                    sprite.OnMouseLeave += ControlEndHighlight;
                    sprite.OnClick += OnControlClick;
                    sprite.Tag = 1;
                    curY += 12;
                }
                curY += elementPadding;
            }
        }
        void ShowTurretUpgrades()
        {
            if (selectedTower == null || selectedTower.turret == null)
                return;

            Turret turret = selectedTower.turret;

            TurretBar.ClearChildren();

            Rectangle innerRect = TurretBar.InnerRect;
            int w2 = innerRect.Width / 2;

            int elementPadding = 8;
            int curY = innerRect.Y + elementPadding;
            // ico
            Sprite sprite = GUI.AddSprite("ico", new Rectangle(innerRect.X + elementPadding, curY, 32, 32), TileSet.Texture, TurretBar);
            sprite.Source = TileSet.GetSourceRect(turret.Specs.gID);
            // sell text
            int txtX = (int)Main.fontSmall.MeasureString("Sell turret:").X;
            TextArea text = GUI.AddTextArea("sell_text", "Sell turret:", new Rectangle(innerRect.Right - 16 - elementPadding - 32 - txtX, curY + 10, txtX, 12), TurretBar);
            text.Font = Main.fontSmall;
            text = GUI.AddTextArea("sell_val", String.Format("{0}", (turret.Cost + turret.ProjSpecs.Cost) / 2f), new Rectangle(innerRect.Right - 16 - elementPadding - 32, curY + 10, 32, 12), TurretBar);
            text.Font = Main.fontNumbers;
            text.Color = Color.Green;
            // sell button
            sprite = GUI.AddSprite("sell", new Rectangle(innerRect.Right - 16 - elementPadding, curY + 8, 16, 16), TileSet.Texture, TurretBar);
            sprite.Source = TileSet.GetSourceRect(sellGID);
            sprite.OnClick += OnControlClick;
            sprite.OnMouseOver += ControlHighlight;
            sprite.OnMouseLeave += ControlEndHighlight;
            sprite.Tag = 2;
            curY += 32 + elementPadding;

            //########### FIRE RATE ##################
            text = GUI.AddTextArea("fr_text", "Fire rate:", new Rectangle(innerRect.X, curY, w2, 12), TurretBar);
            text.Font = Main.fontSmall;
            text.Alignment = TextAlignment.Right;
            text = GUI.AddTextArea("fr_val", String.Format("{0}", turret.FireRate), new Rectangle(innerRect.Center.X + elementPadding, curY, w2, 12), TurretBar);
            text.Font = Main.fontNumbers;
            text.Alignment = TextAlignment.Left;
            text.Color = Color.DarkBlue;
            curY += 14;
            if (turret.FireRateUpgradeIndx < turret.Specs.FireRateUpgrades.Length)
            {
                //upgrade text & icon
                string upcounttxt;
                if (turret.FireRateUpgradeIndx == 0)
                    upcounttxt = "1st";
                else if (turret.FireRateUpgradeIndx == 1)
                    upcounttxt = "2nd";
                else if (turret.FireRateUpgradeIndx == 2)
                    upcounttxt = "3rd";
                else
                    upcounttxt = String.Format("{0}th", turret.FireRateUpgradeIndx + 1);
                text = GUI.AddTextArea("up_text", upcounttxt, new Rectangle(innerRect.X, curY, w2 - 14, 12), TurretBar);
                text.Font = Main.fontSmall;
                text.Alignment = TextAlignment.Right;
                text.Color = Color.Gray;
                sprite = GUI.AddSprite("ico", new Rectangle(innerRect.Center.X - 12, curY, 12, 12), TileSet.Texture, TurretBar);
                sprite.Source = TileSet.GetSourceRect(upgradeGID);

                var up = turret.Specs.FireRateUpgrades[turret.FireRateUpgradeIndx];
                // modifier
                text = GUI.AddTextArea("up_val", String.Format("{0}", up.Item2), new Rectangle(innerRect.Center.X + elementPadding, curY, w2, 12), TurretBar);
                text.Font = Main.fontNumbers;
                text.Color = Color.Green;
                text.Alignment = TextAlignment.Left;

                // price
                text = GUI.AddTextArea("cost", String.Format("{0}", up.Item1), new Rectangle(innerRect.Right - 12 - elementPadding - 32, curY, 32, 12), TurretBar);
                text.Font = Main.fontNumbers;
                if (money < up.Item1)
                    text.Color = Color.DarkRed;
                else
                    text.Color = Color.Goldenrod;
                // buy button
                sprite = GUI.AddSprite("upgradeTurret", new Rectangle(innerRect.Right - 12 - elementPadding, curY, 12, 12), TileSet.Texture, TurretBar);
                sprite.Source = TileSet.GetSourceRect(buyGID);
                sprite.OnMouseOver += ControlHighlight;
                sprite.OnMouseLeave += ControlEndHighlight;
                sprite.OnClick += OnControlClick;
                sprite.Tag = 0;
                curY += 12;
            }
            curY += elementPadding;

            //########### RANGE ##################
            text = GUI.AddTextArea("range_text", "Range:", new Rectangle(innerRect.X, curY, w2, 12), TurretBar);
            text.Font = Main.fontSmall;
            text.Alignment = TextAlignment.Right;
            text = GUI.AddTextArea("range_val", String.Format("{0}", turret.Range), new Rectangle(innerRect.Center.X + elementPadding, curY, w2, 12), TurretBar);
            text.Font = Main.fontNumbers;
            text.Alignment = TextAlignment.Left;
            text.Color = Color.DarkBlue;
            curY += 14;
            if (turret.RangeUpgradeIndx < turret.Specs.RangeUpgrades.Length)
            {
                //upgrade text & icon
                string upcounttxt;
                if (turret.RangeUpgradeIndx == 0)
                    upcounttxt = "1st";
                else if (turret.RangeUpgradeIndx == 1)
                    upcounttxt = "2nd";
                else if (turret.RangeUpgradeIndx == 2)
                    upcounttxt = "3rd";
                else
                    upcounttxt = String.Format("{0}th", turret.RangeUpgradeIndx + 1);
                text = GUI.AddTextArea("up_text", upcounttxt, new Rectangle(innerRect.X, curY, w2 - 14, 12), TurretBar);
                text.Font = Main.fontSmall;
                text.Alignment = TextAlignment.Right;
                text.Color = Color.Gray;
                sprite = GUI.AddSprite("ico", new Rectangle(innerRect.Center.X - 12, curY, 12, 12), TileSet.Texture, TurretBar);
                sprite.Source = TileSet.GetSourceRect(upgradeGID);

                var up = turret.Specs.RangeUpgrades[turret.RangeUpgradeIndx];
                // modifier
                text = GUI.AddTextArea("up_val", String.Format("+{0}", up.Item2), new Rectangle(innerRect.Center.X + elementPadding, curY, w2, 12), TurretBar);
                text.Font = Main.fontNumbers;
                text.Color = Color.Green;
                text.Alignment = TextAlignment.Left;

                // price
                text = GUI.AddTextArea("cost", String.Format("{0}", up.Item1), new Rectangle(innerRect.Right - 12 - elementPadding - 32, curY, 32, 12), TurretBar);
                text.Font = Main.fontNumbers;
                if (money < up.Item1)
                    text.Color = Color.DarkRed;
                else
                    text.Color = Color.Goldenrod;
                // buy button
                sprite = GUI.AddSprite("upgradeTurret", new Rectangle(innerRect.Right - 12 - elementPadding, curY, 12, 12), TileSet.Texture, TurretBar);
                sprite.Source = TileSet.GetSourceRect(buyGID);
                sprite.OnMouseOver += ControlHighlight;
                sprite.OnMouseLeave += ControlEndHighlight;
                sprite.OnClick += OnControlClick;
                sprite.Tag = 1;
                curY += 12;
            }
            curY += elementPadding;

            //########### Damage ##################
            if (turret.DamageMod != 0 || turret.Specs.DamageUpgrades.Length != 0)
            {
                text = GUI.AddTextArea("damage_text", "Damage:", new Rectangle(innerRect.X, curY, w2, 12), TurretBar);
                text.Font = Main.fontSmall;
                text.Alignment = TextAlignment.Right;
                text = GUI.AddTextArea("damage_val", String.Format("{0}", turret.DamageMod), new Rectangle(innerRect.Center.X + elementPadding, curY, w2, 12), TurretBar);
                text.Font = Main.fontNumbers;
                text.Alignment = TextAlignment.Left;
                text.Color = Color.DarkBlue;
                curY += 14;
                if (turret.DamageUpgradeIndx < turret.Specs.DamageUpgrades.Length)
                {
                    //upgrade text & icon
                    string upcounttxt;
                    if (turret.DamageUpgradeIndx == 0)
                        upcounttxt = "1st";
                    else if (turret.DamageUpgradeIndx == 1)
                        upcounttxt = "2nd";
                    else if (turret.DamageUpgradeIndx == 2)
                        upcounttxt = "3rd";
                    else
                        upcounttxt = String.Format("{0}th", turret.DamageUpgradeIndx + 1);
                    text = GUI.AddTextArea("up_text", upcounttxt, new Rectangle(innerRect.X, curY, w2 - 14, 12), TurretBar);
                    text.Font = Main.fontSmall;
                    text.Alignment = TextAlignment.Right;
                    text.Color = Color.Gray;
                    sprite = GUI.AddSprite("ico", new Rectangle(innerRect.Center.X - 12, curY, 12, 12), TileSet.Texture, TurretBar);
                    sprite.Source = TileSet.GetSourceRect(upgradeGID);

                    var up = turret.Specs.DamageUpgrades[turret.DamageUpgradeIndx];
                    // modifier
                    text = GUI.AddTextArea("up_val", String.Format("+{0}", up.Item2), new Rectangle(innerRect.Center.X + elementPadding, curY, w2, 12), TurretBar);
                    text.Font = Main.fontNumbers;
                    text.Color = Color.Green;
                    text.Alignment = TextAlignment.Left;

                    // price
                    text = GUI.AddTextArea("cost", String.Format("{0}", up.Item1), new Rectangle(innerRect.Right - 12 - elementPadding - 32, curY, 32, 12), TurretBar);
                    text.Font = Main.fontNumbers;
                    if (money < up.Item1)
                        text.Color = Color.DarkRed;
                    else
                        text.Color = Color.Goldenrod;
                    // buy button
                    sprite = GUI.AddSprite("upgradeTurret", new Rectangle(innerRect.Right - 12 - elementPadding, curY, 12, 12), TileSet.Texture, TurretBar);
                    sprite.Source = TileSet.GetSourceRect(buyGID);
                    sprite.OnMouseOver += ControlHighlight;
                    sprite.OnMouseLeave += ControlEndHighlight;
                    sprite.OnClick += OnControlClick;
                    sprite.Tag = 2;
                    curY += 12;
                }
                curY += elementPadding;
            }

            //########### VELOCITY ##################
            if (turret.VelocityMod != 0 || turret.Specs.VelocityUpgrades.Length != 0)
            {
                text = GUI.AddTextArea("velocity_text", "Velocity:", new Rectangle(innerRect.X, curY, w2, 12), TurretBar);
                text.Font = Main.fontSmall;
                text.Alignment = TextAlignment.Right;
                text = GUI.AddTextArea("velocity_val", String.Format("{0}", turret.VelocityMod), new Rectangle(innerRect.Center.X + elementPadding, curY, w2, 12), TurretBar);
                text.Font = Main.fontNumbers;
                text.Alignment = TextAlignment.Left;
                text.Color = Color.DarkBlue;
                curY += 14;
                if (turret.VelocityUpgradeIndx < turret.Specs.VelocityUpgrades.Length)
                {
                    //upgrade text & icon
                    string upcounttxt;
                    if (turret.VelocityUpgradeIndx == 0)
                        upcounttxt = "1st";
                    else if (turret.VelocityUpgradeIndx == 1)
                        upcounttxt = "2nd";
                    else if (turret.VelocityUpgradeIndx == 2)
                        upcounttxt = "3rd";
                    else
                        upcounttxt = String.Format("{0}th", turret.VelocityUpgradeIndx + 1);
                    text = GUI.AddTextArea("up_text", upcounttxt, new Rectangle(innerRect.X, curY, w2 - 14, 12), TurretBar);
                    text.Font = Main.fontSmall;
                    text.Alignment = TextAlignment.Right;
                    text.Color = Color.Gray;
                    sprite = GUI.AddSprite("ico", new Rectangle(innerRect.Center.X - 12, curY, 12, 12), TileSet.Texture, TurretBar);
                    sprite.Source = TileSet.GetSourceRect(upgradeGID);

                    var up = turret.Specs.VelocityUpgrades[turret.VelocityUpgradeIndx];
                    // modifier
                    text = GUI.AddTextArea("up_val", String.Format("+{0}", up.Item2), new Rectangle(innerRect.Center.X + elementPadding, curY, w2, 12), TurretBar);
                    text.Font = Main.fontNumbers;
                    text.Color = Color.Green;
                    text.Alignment = TextAlignment.Left;

                    // price
                    text = GUI.AddTextArea("cost", String.Format("{0}", up.Item1), new Rectangle(innerRect.Right - 12 - elementPadding - 32, curY, 32, 12), TurretBar);
                    text.Font = Main.fontNumbers;
                    if (money < up.Item1)
                        text.Color = Color.DarkRed;
                    else
                        text.Color = Color.Goldenrod;
                    // buy button
                    sprite = GUI.AddSprite("upgradeTurret", new Rectangle(innerRect.Right - 12 - elementPadding, curY, 12, 12), TileSet.Texture, TurretBar);
                    sprite.Source = TileSet.GetSourceRect(buyGID);
                    sprite.OnMouseOver += ControlHighlight;
                    sprite.OnMouseLeave += ControlEndHighlight;
                    sprite.OnClick += OnControlClick;
                    sprite.Tag = 3;
                    curY += 12;
                }
                curY += elementPadding;
            }
        
        }
        void ShowProjectileInfo()
        {
            if (selectedTower == null || selectedTower.turret == null || selectedTower.turret.ProjSpecs.ID == -1)
                return;

            ProjectileBar.ClearChildren();

            ProjectileSpecs spec = selectedTower.turret.ProjSpecs;
            Rectangle innerRect = ProjectileBar.InnerRect;
            int w2 = innerRect.Width / 2;

            int elementPadding = 8;
            int curY = innerRect.Y + elementPadding;
            // ico
            Sprite sprite = GUI.AddSprite("ico", new Rectangle(innerRect.X + elementPadding, curY, 32, 32), TileSet.Texture, ProjectileBar);
            sprite.Source = TileSet.GetSourceRect(selectedTower.turret.ProjSpecs.gID);
            // sell text
            int txtX = (int)Main.fontSmall.MeasureString("Sell projectile:").X;
            TextArea text = GUI.AddTextArea("sell_text", "Sell projectile:", new Rectangle(innerRect.Right - 16 - elementPadding - 32 - txtX, curY + 10, txtX, 12), ProjectileBar);
            text.Font = Main.fontSmall;
            text = GUI.AddTextArea("sell_val", String.Format("{0}", selectedTower.turret.ProjSpecs.Cost / 2), new Rectangle(innerRect.Right - 16 - elementPadding - 32, curY + 10, 32, 12), ProjectileBar);
            text.Font = Main.fontNumbers;
            text.Color = Color.Green;
            // sell button
            sprite = GUI.AddSprite("sell", new Rectangle(innerRect.Right - 16 - elementPadding, curY + 8, 16, 16), TileSet.Texture, ProjectileBar);
            sprite.Source = TileSet.GetSourceRect(sellGID);
            sprite.OnClick += OnControlClick;
            sprite.OnMouseOver += ControlHighlight;
            sprite.OnMouseLeave += ControlEndHighlight;
            sprite.Tag = 3;
            curY += 32 + elementPadding;

            if (spec.Damage != 0)
            {
                text = GUI.AddTextArea("damage_text", "Damage:", new Rectangle(innerRect.X, curY, w2, 12), ProjectileBar);
                text.Font = Main.fontSmall;
                text.Alignment = TextAlignment.Right;
                text = GUI.AddTextArea("damage_val", String.Format("{0}", spec.Damage), new Rectangle(innerRect.Center.X + elementPadding, curY, w2, 12), ProjectileBar);
                text.Font = Main.fontNumbers;
                text.Color = Color.DarkBlue;
                text.Alignment = TextAlignment.Left;
                curY += 12 + elementPadding;
            }
            if (spec.DamageRadius != 0)
            {
                text = GUI.AddTextArea("radius_text", "Radius:", new Rectangle(innerRect.X, curY, w2, 12), ProjectileBar);
                text.Font = Main.fontSmall;
                text.Alignment = TextAlignment.Right;
                text = GUI.AddTextArea("radius_val", String.Format("{0}", spec.DamageRadius), new Rectangle(innerRect.Center.X + elementPadding, curY, w2, 12), ProjectileBar);
                text.Font = Main.fontNumbers;
                text.Color = Color.DarkBlue;
                text.Alignment = TextAlignment.Left;
                curY += 12 + elementPadding;
            }
            text = GUI.AddTextArea("velocity_text", "Velocity:", new Rectangle(innerRect.X, curY, w2, 12), ProjectileBar);
            text.Font = Main.fontSmall;
            text.Alignment = TextAlignment.Right;
            text = GUI.AddTextArea("velocity_val", String.Format("{0}", spec.Velocity), new Rectangle(innerRect.Center.X + elementPadding, curY, w2, 12), ProjectileBar);
            text.Font = Main.fontNumbers;
            text.Color = Color.DarkBlue;
            text.Alignment = TextAlignment.Left;
            curY += 12 + elementPadding;

            if (spec.RangeMod != 0)
            {
                text = GUI.AddTextArea("range_text", "Range:", new Rectangle(innerRect.X, curY, w2, 12), ProjectileBar);
                text.Font = Main.fontSmall;
                text.Alignment = TextAlignment.Right;
                text = GUI.AddTextArea("range_val", String.Format("+{0}", spec.RangeMod), new Rectangle(innerRect.Center.X + elementPadding, curY, w2, 12), ProjectileBar);
                text.Font = Main.fontNumbers;
                text.Color = Color.DarkBlue;
                text.Alignment = TextAlignment.Left;
                curY += 12 + elementPadding;
            }

            if (spec.Buff != 0)
            {
                string effect = "Slow:";
                if (spec.Buff == 2)
                    effect = "Weak:";
                else if (spec.Buff == 3)
                    effect = "Dmg over time";
                text = GUI.AddTextArea("buff_text", effect, new Rectangle(innerRect.X, curY, w2, 12), ProjectileBar);
                text.Font = Main.fontSmall;
                text.Alignment = TextAlignment.Right;
                text = GUI.AddTextArea("buff_val", String.Format("{0}", spec.BuffMod), new Rectangle(innerRect.X + w2 + elementPadding, curY, w2, 12), ProjectileBar);
                text.Font = Main.fontNumbers;
                text.Alignment = TextAlignment.Left;
                text.Color = Color.DarkGreen;
                text = GUI.AddTextArea("buff_val", String.Format("{0}s", spec.BuffTime), new Rectangle(text.Position.X + (int)Main.fontNumbers.MeasureString(text.Text).X + 8, curY, w2, 12), ProjectileBar);
                text.Font = Main.fontNumbers;
                text.Alignment = TextAlignment.Left;
                if (spec.BuffNTime != 0)
                {
                    text = GUI.AddTextArea("buff_val", String.Format("{0}s", spec.BuffNTime), new Rectangle(text.Position.X + (int)Main.fontNumbers.MeasureString(text.Text).X + 8, curY, w2, 12), ProjectileBar);
                    text.Font = Main.fontNumbers;
                    text.Alignment = TextAlignment.Left;
                }
                curY += 12 + elementPadding;
            }
        }
        void ShowStatusBar()
        {
            StatusBar.ClearChildren();
            Rectangle inner = StatusBar.InnerRect;
            int w2 = inner.Width / 2;
            int elementPadding = 8;
            int elementH = 24;

            TextArea text = GUI.AddTextArea("statusbar", "Status", new Rectangle(inner.X, inner.Y, inner.Width, elementH), StatusBar);
            text.Color = Color.Gray;

            Button b = GUI.AddButton("menu", null, new Rectangle(inner.Right - 32, inner.Top, 32, 32), OnButtonClick, (int)GUI_Button_Style.Menu, StatusBar);  

            int curY = inner.Y + elementH + elementPadding;
            text = GUI.AddTextArea("wave", "Wave:", new Rectangle(inner.X, curY, w2, elementH), StatusBar);
            text.Alignment = TextAlignment.Right;
            text = GUI.AddTextArea("wave_val", String.Format("{0}/{1}", currentWave, waveCount), new Rectangle(inner.Center.X + elementPadding, curY, w2, elementH), StatusBar);
            text.Font = Main.fontNumbers;
            text.Alignment = TextAlignment.Left;
            text.Color = Color.DarkBlue;

            curY += elementH + elementPadding / 2;
            text = GUI.AddTextArea("hp", "Health:", new Rectangle(inner.X, curY, w2, elementH), StatusBar);
            text.Alignment = TextAlignment.Right;
            text = GUI.AddTextArea("hp_val", String.Format("{0}", BaseHP), new Rectangle(inner.Center.X + elementPadding, curY, w2, elementH), StatusBar);
            text.Font = Main.fontNumbers;
            text.Alignment = TextAlignment.Left;
            text.Color = Color.DarkRed;

            curY += elementH + elementPadding / 2;
            text = GUI.AddTextArea("money", "Money:", new Rectangle(inner.X, curY, w2, elementH), StatusBar);
            text.Alignment = TextAlignment.Right;
            text = GUI.AddTextArea("money_val", String.Format("{0}", money), new Rectangle(inner.Center.X + elementPadding, curY, w2, elementH), StatusBar);
            text.Font = Main.fontNumbers;
            text.Alignment = TextAlignment.Left;
            text.Color = Color.DarkGreen;      
        }
        void UpdateStatusBar()
        {
            if (StatusBar == null)
                return;
            TextArea hp = StatusBar.FindChild("hp_val", false) as TextArea;
            if (hp != null)
                hp.Text = BaseHP.ToString();
            TextArea mtxt = StatusBar.FindChild("money_val", false) as TextArea;
            if (mtxt != null)
                mtxt.Text = money.ToString();
            TextArea waveTxt = StatusBar.FindChild("wave_val", false) as TextArea;
            if (waveTxt != null)
                waveTxt.Text = String.Format("{0}/{1}", currentWave, waveCount);
        }

 
        void SetUpLevel()
        {
            lvlWidth = level.Width;
            lvlHeight = level.Height;

            tileW = GameAreaWidth / lvlWidth;
            tileH = GameAreaHeight / lvlHeight;
            if (tileH < tileW)
                tileW = tileH;
            else
                tileH = tileW;

            paddingX = (GameAreaWidth - tileW * lvlWidth) / 2;
            paddingY = (GameAreaHeight - tileH * lvlHeight) / 2;

            Color[] colors = new Color[lvlWidth * lvlHeight];
            level.GetData(colors);

            ground = new ushort[lvlWidth * lvlHeight];
            road = new ushort[lvlWidth * lvlHeight];
            scenery = new ushort[lvlWidth * lvlHeight];
            path = new BitArray(lvlWidth * lvlHeight);

            for (int y = 0; y < lvlHeight; y++)
            {
                for (int x = 0; x < lvlWidth; x++)
                {
                    int tile = x + y * lvlWidth;
                    if (colors[tile] == Color.Black) // path
                    {
                        road[tile] = 22;
                        path[tile] = true;
                    }
                    else if (colors[tile] == Color.Red) // invasion start
                    {
                        startTile = (ushort)tile;
                        path[tile] = true;
                    }
                    else if (colors[tile].G == 255 && colors[tile].R == 0 && colors[tile].B == 0) // green - player's base
                    {
                        endTile = (ushort)tile;
                        path[tile] = true;
                    }
                    else if (colors[tile] == Color.Blue) // buildable area
                    {
                        ground[tile] = 1;
                    }
                    else // normal ground tile
                    {
                        // add random scenery
                        if (random.Next(0, 100) < 4)
                        {
                            scenery[tile] = sceneryGIDs[random.Next(0, sceneryGIDs.Length)];
                        }
                    }
                }
            }
            ProcessRoads();
            ProcessGroundEndTiles();
            nextNpcs = GenerateNpcWave(0);
            UpdateWaveBar();
        }
        void ProcessRoads()
        {
            for (int y = 0; y < lvlHeight; y++)
            {
                for (int x = 0; x < lvlWidth; x++)
                {
                    int tile = x + y * lvlWidth;
                    if (path[tile])
                    {
                        Direction connection = PathConnection(x, y);
                        ushort gid = pathDirectionGIDtable[(byte)connection];
                        if (gid != 0)
                        {
                            road[tile] = gid;

                        }

                        else // dead end, check direction of start/end
                        {
                            Direction conn2 = ConnectedDirection((ushort)tile, startTile);
                            if (conn2 != Direction.None)
                                road[tile] = pathDirectionGIDtable[(byte)(connection | conn2)];
                            else
                            {
                                conn2 = ConnectedDirection((ushort)tile, endTile);
                                road[tile] = pathDirectionGIDtable[(byte)(connection | conn2)];
                            }                            
                        }
                    }
                }
            }
        }
        void ProcessGroundEndTiles()
        {
            for (int y = 0; y < lvlHeight; y++)
            {
                for (int x = 0; x < lvlWidth; x++)
                {
                    int tile = x + y * lvlWidth;
                    if (ground[tile] == 0) // empty non-buildable tile
                    {
                        Direction connection = GeneralConnection(x, y, 1, ref ground);

                        ushort gid = groundDirectionGIDtable[(byte)connection];
                        if (gid != 0)
                        {
                            ground[tile] = gid;
                        }
                    }
                }
            }
        }
        Direction PathConnection(int tile)
        {
            return PathConnection(tile % lvlWidth, tile / lvlWidth);
        }
        Direction PathConnection(int x, int y)
        {
            int tile = x + y * lvlWidth;
            Direction d = Direction.None;
            if (y > 0 && path[tile - lvlWidth])
                d |= Direction.Up;
            if (y < lvlHeight - 1 && path[tile + lvlWidth])
                d |= Direction.Down;
            if (x > 0 && path[tile - 1])
                d |= Direction.Left;
            if (x < lvlWidth - 1 && path[tile + 1])
                d |= Direction.Right;
            return d;
        }
        Direction GeneralConnection(int x, int y, ushort target, ref ushort[] data)
        {
            ushort tile = (ushort)(x + y * lvlWidth);
            Direction d = Direction.None;
            if (y > 0 && data[tile - lvlWidth] == target)
                d |= Direction.Up;
            if (y < lvlHeight - 1 && data[tile + lvlWidth] == target)
                d |= Direction.Down;
            if (x > 0 && data[tile - 1] == target)
                d |= Direction.Left;
            if (x < lvlWidth - 1 && data[tile + 1] == target)
                d |= Direction.Right;
            return d;
        }
        Direction ConnectedDirection(ushort origin, ushort target)
        {
            Direction d = Direction.None;
            int xO = origin % lvlWidth;
            int yO = origin / lvlWidth;
            int xT = target % lvlWidth;
            int yT = target / lvlWidth;
            if (xO == xT)
            {
                if (yO == yT + 1)
                    d = Direction.Up;
                else if (yO == yT - 1)
                    d = Direction.Down;                
            }
            else if (yO == yT)
            {
                if (xO == xT + 1)
                    d = Direction.Left;
                else if (xO == xT - 1)
                    d = Direction.Right;
            }
            return d;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }



        void OnSpeedChange(object sender, EventArgs args)
        {
            Slider s = sender as Slider;
            float newSpeed = (float)Math.Round(MathHelper.Lerp(1, 12, s.Value), 1);
            TextArea speedTxt = ControlBar.FindChild("speedtxt", false) as TextArea;
            if (speedTxt != null)
                speedTxt.Text = String.Format("Speed: {0}x", newSpeed);
            if (speedMod != 0) // adjust speed if not paused
                speedMod = newSpeed;
        }
        /// <summary>
        /// get the instance of Tower or NULL at the specified tile
        /// </summary>
        /// <param name="tile"></param>
        /// <returns></returns>
        Tower TowerAtTile(int tile)
        {
            foreach (Tower t in towerList)
                if (t.Tile == tile)
                    return t;
            return null;
        }
        void OnControlClick(Object sender, EventArgs args)
        {
            if (ended)
                return;
            Control c = sender as Control;
            switch (c.Name)
            {
                case "controller":
                    {
                        if (mouseTile == -1)
                            break;
                        Tower tower = TowerAtTile(mouseTile);

                        if (placingTower) // Place NEW tower
                        {
                            if (ground[mouseTile] != 1 || tower != null)
                            {
                                Main.Audio.PlaySound("error");
                                animatedText.Add(new TranText("Can't build here", new Vector2(oldMouseState.X, oldMouseState.Y - 16), 0.75f, new Vector2(0, -40), Color.Red));
                            }
                            else if (selectedTower.Cost > money)
                            {
                                Main.Audio.PlaySound("error");
                                animatedText.Add(new TranText("Not enough money!", new Vector2(oldMouseState.X, oldMouseState.Y - 16), 0.75f, new Vector2(0, -40), Color.Red));
                            }
                            else // buildable & empty tile & enough money
                            {
                                Money -= selectedTower.Cost; // deduce cost
                                animatedText.Add(new TranText(String.Format("- {0}$",selectedTower.Cost), new Vector2(oldMouseState.X, oldMouseState.Y - 16), 1f, new Vector2(0, -30), Color.Goldenrod));
                                selectedTower.Tile = (ushort)mouseTile; // assign tile
                                towerList.Add(selectedTower); // add to active tower list
                                CancelSelection();
                                Main.Audio.PlaySound("sell");
                            }
                        }
                        else if (tower != null)
                        {
                            if (tower != selectedTower) // select the tower at mouse click
                            {
                                selectedTower = tower;
                                UpdateTowerInfo(); // update tower's info in GUI
                            }
                        }
                        else if (selectedTower != null)
                            CancelSelection();

                        break;
                    }
                case "platform":
                    {
                        placingTower = true;
                        TowerPlatform platform = new TowerPlatform(TowerPlatformSpecs.AllAvailable[(int)c.Tag]);
                        if (selectedTower == null)
                            selectedTower = new Tower(platform, null, this);
                        else
                            selectedTower.platform = platform;
                        ShowPlatformChoice();
                        ShowTurretChoice();
                        break;
                    }
                case "turret":
                    {
                        int specID = (int)c.Tag;
                        if (placingTower)
                            selectedTower.turret = new Turret(TurretSpecs.AllAvailable[specID]);
                        else
                            selectedTurret = specID;
                        ShowTurretChoice();
                        ShowProjectileChoice();
                        break;
                    }
                case "projectile":
                    {
                        int specID = (int)c.Tag;
                        if (placingTower)
                            selectedTower.turret.ProjSpecs = ProjectileSpecs.AllAvailable[specID];
                        else
                            selectedProj = specID;
                        ShowProjectileChoice();
                        break;
                    }
                case "sell":
                    {
                        int id = (int)c.Tag;
                        int m = 0;
                        if (id == 1) // sell whole tower
                        {
                            m = selectedTower.Cost;
                            towerList.Remove(selectedTower);
                            Money += m / 2;
                            animatedText.Add(new TranText(string.Format("+ {0}$", m / 2), selectedTower.Position, 1, new Vector2(0, -30), Color.Green));
                            Main.Audio.PlaySound("sell");
                            CancelSelection();
                            break;
                        }
                        else if (id == 2) // sell turret 
                        {
                            m = selectedTower.turret.Cost + selectedTower.turret.ProjSpecs.Cost;
                            selectedTower.turret = null;
                            
                        }
                        else if (id == 3) // sell projectile
                        {
                            m = selectedTower.turret.ProjSpecs.Cost;
                            selectedTower.turret.ProjSpecs = ProjectileSpecs.Null;
                        }
                        Money += m / 2;
                        animatedText.Add(new TranText(string.Format("+ {0}$", m / 2), selectedTower.Position, 1, new Vector2(0, -30), Color.Green));
                        Main.Audio.PlaySound("sell");
                        UpdateTowerInfo();
                        break;
                    }
                case "buyturret":
                    {
                        TurretSpecs spec = TurretSpecs.AllAvailable[selectedTurret];
                        if (spec.Cost <= money)
                        {
                            selectedTower.turret = new Turret(spec);
                            animatedText.Add(new TranText(string.Format("- {0}$", spec.Cost), new Vector2(oldMouseState.X - 100, oldMouseState.Y), 0.75f, new Vector2(0, -30), Color.Goldenrod));
                            Main.Audio.PlaySound("sell");
                            Money -= spec.Cost;
                            UpdateTowerInfo();
                        }
                        else
                        {
                            Main.Audio.PlaySound("error");
                            animatedText.Add(new TranText("Not enough money", new Vector2(oldMouseState.X - 200, oldMouseState.Y - 32), 0.75f, new Vector2(0, -30), Color.DarkRed)); 
                        }
                        break;
                    }
                case "buyproj":
                    {
                        ProjectileSpecs spec = ProjectileSpecs.AllAvailable[selectedProj];
                        if (spec.Cost <= money)
                        {
                            selectedTower.turret.ProjSpecs = spec;
                            animatedText.Add(new TranText(string.Format("- {0}$", spec.Cost), new Vector2(oldMouseState.X - 100, oldMouseState.Y), 0.75f, new Vector2(0, -30), Color.Goldenrod));
                            Main.Audio.PlaySound("sell");
                            Money -= spec.Cost;
                            UpdateTowerInfo();
                        }
                        else
                        {
                            Main.Audio.PlaySound("error");
                            animatedText.Add(new TranText("Not enough money", new Vector2(oldMouseState.X - 200, oldMouseState.Y), 0.75f, new Vector2(0, -30), Color.DarkRed)); 
                        }
                        break;
                    }                    
                case "upgradePlatform":
                    {
                        int id = (int)c.Tag;
                        bool success = false;
                        string upText = "";
                        if (id == 0) // fire rate upgrade
                        {
                            int uIndx = selectedTower.platform.FireRateUpgradeIndx;
                            if (uIndx >= selectedTower.platform.Specs.FireRateUpgrades.Length)
                                break;
                            var data = selectedTower.platform.Specs.FireRateUpgrades[uIndx];
                            if (data.Item1 <= money)
                            {
                                selectedTower.platform.FireRateMod += data.Item2;
                                selectedTower.platform.FireRateUpgradeIndx++;
                                upText = String.Format("+ {0}", data.Item2);
                                Money -= data.Item1;
                                success = true;
                            }
                        }
                        else if (id == 1) // range upgrade
                        {
                            int uIndx = selectedTower.platform.RangeUpgradeIndx;
                            if (uIndx >= selectedTower.platform.Specs.RangeUpgrades.Length)
                                break;
                            var data = selectedTower.platform.Specs.RangeUpgrades[uIndx];
                            if (data.Item1 <= money)
                            {
                                selectedTower.platform.RangeMod += data.Item2;
                                selectedTower.platform.RangeUpgradeIndx++;
                                upText = String.Format("+ {0}", data.Item2);
                                Money -= data.Item1;
                                success = true;
                            }
                        }
                        if (success)
                        {
                            Main.Audio.PlaySound("upgrade");
                            animatedText.Add(new TranText(upText, new Vector2(oldMouseState.X - 50, oldMouseState.Y), 0.75f, new Vector2(0, -30), Color.DarkBlue));
                            UpdateTowerInfo();
                        }
                        else
                        {
                            Main.Audio.PlaySound("error");
                            animatedText.Add(new TranText("Not enough money", new Vector2(oldMouseState.X - 200, oldMouseState.Y), 0.75f, new Vector2(0, -30), Color.DarkRed)); 
                        }
                        break;
                    }
                case "upgradeTurret":
                    {
                        int id = (int)c.Tag;
                        bool success = false;
                        string upText = "";
                        if (id == 0) // fire rate upgrade
                        {
                            int uIndx = selectedTower.turret.FireRateUpgradeIndx;
                            if (uIndx >= selectedTower.turret.Specs.FireRateUpgrades.Length)
                                break;
                            var data = selectedTower.turret.Specs.FireRateUpgrades[uIndx];
                            if (data.Item1 <= money)
                            {
                                selectedTower.turret.FireRateMod += data.Item2;
                                selectedTower.turret.FireRateUpgradeIndx++;
                                upText = String.Format("+ {0}", data.Item2);
                                Money -= data.Item1;
                                success = true;
                            }
                        }
                        else if (id == 1) // range upgrade
                        {
                            int uIndx = selectedTower.turret.RangeUpgradeIndx;
                            if (uIndx >= selectedTower.turret.Specs.RangeUpgrades.Length)
                                break;
                            var data = selectedTower.turret.Specs.RangeUpgrades[uIndx];
                            if (data.Item1 <= money)
                            {
                                selectedTower.turret.RangeMod += data.Item2;
                                selectedTower.turret.RangeUpgradeIndx++;
                                upText = String.Format("+ {0}", data.Item2);
                                Money -= data.Item1;
                                success = true;
                            }
                        }
                        else if (id == 2) // damage upgrade
                        {
                            int uIndx = selectedTower.turret.DamageUpgradeIndx;
                            if (uIndx >= selectedTower.turret.Specs.DamageUpgrades.Length)
                                break;
                            var data = selectedTower.turret.Specs.DamageUpgrades[uIndx];
                            if (data.Item1 <= money)
                            {
                                selectedTower.turret.DamageMod += data.Item2;
                                selectedTower.turret.DamageUpgradeIndx++;
                                upText = String.Format("+ {0}", data.Item2);
                                Money -= data.Item1;
                                success = true;
                            }
                        }
                        else if (id == 3) // velocity upgrade
                        {
                            int uIndx = selectedTower.turret.VelocityUpgradeIndx;
                            if (uIndx >= selectedTower.turret.Specs.VelocityUpgrades.Length)
                                break;
                            var data = selectedTower.turret.Specs.VelocityUpgrades[uIndx];
                            if (data.Item1 <= money)
                            {
                                selectedTower.turret.VelocityMod += data.Item2;
                                selectedTower.turret.VelocityUpgradeIndx++;
                                upText = String.Format("+ {0}", data.Item2);
                                Money -= data.Item1;
                                success = true;
                            }
                        }
                        if (success)
                        {
                            Main.Audio.PlaySound("upgrade");
                            animatedText.Add(new TranText(upText, new Vector2(oldMouseState.X - 50, oldMouseState.Y), 0.75f, new Vector2(0, -30), Color.DarkBlue));
                            UpdateTowerInfo();
                        }
                        else
                        {
                            Main.Audio.PlaySound("error");
                            animatedText.Add(new TranText("Not enough money", new Vector2(oldMouseState.X - 200, oldMouseState.Y), 0.75f, new Vector2(0, -30), Color.DarkRed));
                        }
                        break;
                    }

            }
        }
        void UpdateTowerInfo()
        {
            if (selectedTower == null)
            {
                CancelSelection();
                return;
            }
            else if (placingTower)
            {
                ShowPlatformChoice();
                ShowTurretChoice();
                ShowProjectileChoice();
                return;
            }
            
            ShowPlatformUpgrades();
            ProjectileBar.ClearChildren();
            if (selectedTower.turret != null)
            {
                ShowTurretUpgrades();
                if (selectedTower.turret.ProjSpecs.ID != -1)
                    ShowProjectileInfo();
                else
                    ShowProjectileChoice();
            }
            else
                ShowTurretChoice();
        }

        void ControlHighlight(Object sender, EventArgs args)
        {
            Control c = sender as Control;
            int sizemod = (int)(c.Position.Width * 0.1f);
            c.Position = new Rectangle(c.Position.X - sizemod, c.Position.Y - sizemod, c.Position.Width + sizemod * 2, c.Position.Height + sizemod * 2);
            c.Color = Color.Orange;
        }
        void ControlEndHighlight(Object sender, EventArgs args)
        {
            Control c = sender as Control;
            int sizemod = c.Position.Width - (int)Math.Round(c.Position.Width / 1.1f);
            c.Position = new Rectangle(c.Position.X + sizemod, c.Position.Y + sizemod, c.Position.Width - sizemod * 2, c.Position.Height - sizemod * 2);
            c.Color = Color.White;
        }


        void OnButtonClick(Object sender, EventArgs args)
        {
            Button b = sender as Button;
            Main.Audio.PlaySound("click");
            switch (b.Name)
            {
                case "pause":
                    {
                        TogglePause();
                        break;
                    }
                case "resume":
                    {
                        if (speedMod == 0)
                            TogglePause();
                        break;
                    }
                case "settings":
                    {
                        Main main = Game as Main;
                        main.ShowSettings();
                        break;
                    }
                case "exit":
                    {
                        GUI.ClearChildren();
                        TitleScreen title = new TitleScreen(Game);
                        Game.Components.Add(title);
                        Game.Components.Remove(this);
                        this.Dispose(true);
                        break;
                    }
                case "menu":
                    {
                        ToggleMenu();
                        break;
                    }
            }
        }
        void ToggleMenu()
        {
            Control frame = GUI.FindChild("menu", false);
            if (frame == null) // open options menu
            {
                if (speedMod != 0) // if not paused, toggle pause
                    TogglePause();
                int w2 = GraphicsDevice.Viewport.Width / 2;
                int h2 = GraphicsDevice.Viewport.Height / 2;
                frame = GUI.AddFrame("menu", new Rectangle(w2 - 128, h2 - 64, 256, 160), (int)GUI_Frame_Style.Default);
                int h = (frame.Position.Height - 64) / 3;
                GUI.AddButton("menu", "Resume", new Rectangle(frame.Position.X + 16, frame.Position.Y + 16, frame.Position.Width - 32, h), OnButtonClick, (int)GUI_Button_Style.Default, frame);
                GUI.AddButton("settings", "Settings", new Rectangle(frame.Position.X + 16, frame.Position.Y + 32 + h, frame.Position.Width - 32, h), OnButtonClick, (int)GUI_Button_Style.Default, frame);
                GUI.AddButton("exit", "Main menu", new Rectangle(frame.Position.X + 16, frame.Position.Y + 48 + h * 2, frame.Position.Width - 32, h), OnButtonClick, (int)GUI_Button_Style.Default, frame);

            }
            else // close menu
                GUI.Remove(frame);
        }
        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            KeyboardState keyState = Keyboard.GetState();
            MouseState mouseState= Mouse.GetState();

            if (keyState.IsKeyDown(Keys.Escape) && oldKeyState.IsKeyUp(Keys.Escape))
            {
                ToggleMenu();
            }
            else if (keyState.IsKeyDown(Keys.Space) && oldKeyState.IsKeyUp(Keys.Space))            
                TogglePause();
            

            mouseTile = TileAtPosition(new Vector2(mouseState.X, mouseState.Y));
            if (mouseState.RightButton == ButtonState.Pressed && oldMouseState.RightButton == ButtonState.Released)  // right click
                CancelSelection();

            oldKeyState = keyState;
            oldMouseState = mouseState;

            if (ended)
                return;

            float dTReal = (float)gameTime.ElapsedGameTime.Milliseconds / 1000;
            float dT = dTReal * speedMod;

            if (currentWave < waveCount)
            {
                nextWave -= dT;
                if (nextWave <= 0) // spawn new wave
                {
                    currentWave++;
                    nextWave += waveFreq;
                    waveNpcs = nextNpcs;
                    nextNpcs = GenerateNpcWave(difficultyMod);

                    UpdateWaveBar();
                    UpdateStatusBar();
                    difficultyMod += 1f / numOfWaves;

                }
            }            
            if (waveNpcs.Count != 0) // spawn more of current wave
            {
                Npc npc = waveNpcs[0];
                npc.TransAmt -= dT;
                if (npc.TransAmt <= 0)
                {
                    npc.TransAmt = 0;
                    waveNpcs.RemoveAt(0);
                    npcList.Add(npc);
                    UpdateWaveBar(false);
                }
            }
            else if (currentWave == waveCount && npcList.Count == 0 && baseHP > 0) // Player WIN
            {
                EndGame(true);
            }

            clockText.Text = string.Format("{0}s", Math.Round(nextWave, 1)); // update clock text

            for (int i = npcList.Count - 1; i >= 0; i--) // update all active npcs, remove inactive ones
            {
                Npc npc = npcList[i];
                if (npc == null || !npc.Active)
                {
                    npcList.RemoveAt(i);
                    continue;
                }
                npc.Update(dT);
            }
            foreach (Tower tower in towerList) // update all towers
            {
                tower.Update(dT);
            }
            for (int i = projectileList.Count - 1; i >= 0; i--) // update all active projectiles, remove inactive ones
            {
                Projectile projectile = projectileList[i];
                if (projectile == null)                
                    projectileList.RemoveAt(i);
                else if (!projectile.Active) // projectile inactive
                {
                    projectileList.RemoveAt(i);

                    if (projectile.Specs.HitSound == "explosion")
                    {
                        AnimatedSprite sprite = new AnimatedSprite(ExplosionSheet, 0.02f, projectile.Target.Position, false); // add explosion sprite
                        animatedList.Add(sprite);
                    }
                    Main.Audio.PlaySound(projectile.Specs.HitSound);

                    if (projectile.Specs.DamageRadius == 0)
                    {
                        float dmg = projectile.Specs.Damage;
                        foreach (Buff b in projectile.Target.Buffs)
                            if (b.Type == 2) // weakness buff
                                dmg *= b.Mod;
                        projectile.Target.HP -= dmg;
                        if (projectile.Specs.Buff != 0)// add the projectile's buff to npc's buff list
                            projectile.Target.Buffs.Add(new Buff { Type = projectile.Specs.Buff, Duration = projectile.Specs.BuffTime, Mod = projectile.Specs.BuffMod, CurTime = projectile.Specs.BuffNTime, NextTime = projectile.Specs.BuffNTime });
                        if (projectile.Target.Active && projectile.Target.HP <= 0) // npc dies
                        {
                            projectile.Target.Active = false;
                            Money += projectile.Target.Type.MoneyDrop; // add npc drop to money
                        }
                    }
                    else
                    {
                        var npcs = GetNPCsAtPosition(projectile.TilePos, projectile.Specs.DamageRadius);
                        foreach (Npc npc in npcs)
                        {
                            float dmg = projectile.Specs.Damage;
                            foreach (Buff b in npc.Buffs)
                                if (b.Type == 2) // weakness buff
                                    dmg *= b.Mod;
                            npc.HP -= dmg;
                            if (projectile.Specs.Buff != 0) // add the projectile's buff to npc's buff list
                                npc.Buffs.Add(new Buff { Type = projectile.Specs.Buff, Duration = projectile.Specs.BuffTime, Mod = projectile.Specs.BuffMod, CurTime = projectile.Specs.BuffNTime, NextTime = projectile.Specs.BuffNTime });
                            if (npc.Active && npc.HP <= 0) // npc dies
                            {
                                npc.Active = false;
                                Money += npc.Type.MoneyDrop; // add npc drop to money
                            }
                        }
                    }
                }
                else
                    projectile.Update(dT);
            }
            for (int i = animatedList.Count - 1; i >= 0; i--) // update all animated sprites, remove inactive ones
            {
                AnimatedSprite sprite = animatedList[i];
                if (sprite == null || !sprite.Active)
                    animatedList.RemoveAt(i);
                else
                    sprite.Update(dT);
            }
            for (int i = animatedText.Count - 1; i >= 0; i--) // update all animated text, remove inactive ones
            {
                TranText txt = animatedText[i];
                if (txt == null || !txt.Active)
                    animatedText.RemoveAt(i);
                else
                    txt.Update(dTReal);
            }

        }
        void CancelSelection() // cancel tower selection
        {
            placingTower = false;
            selectedTower = null;
            selectedProj = -1;
            selectedTurret = -1;
            ShowPlatformChoice();
            TurretBar.ClearChildren();
            ProjectileBar.ClearChildren();
        }
        void TogglePause()
        {
            TextArea txt = ControlBar.FindChild("pausetxt", false) as TextArea;
            if (speedMod == 0 && !ended) 
            {
                // Unpause 
                Slider s = ControlBar.FindChild("speedSlider", false) as Slider;
                if (s != null)
                    speedMod = (float)Math.Round(MathHelper.Lerp(1, 12, s.Value), 1);
                else
                    speedMod = 1f;
                txt.Text = "Pause";
                txt.Color = Color.Black;
            }
            else
            {
                // Pause 
                txt.Text = "Paused";
                txt.Color = Color.Red;
                speedMod = 0;
            }
        }
        void UpdateWaveBar(bool full = true)
        {
            int tileH;
            if (full)
            {
                Control c = GUI.FindChild("nextwave", false);
                if (c != null)
                {
                    c.ClearChildren();
                    int x = c.Position.X + 16;
                    int y = c.Position.Y + 16;
                    tileH = c.Position.Height;
                    var group = nextNpcs.GroupBy(n => n.Type.ID).Select(f => new { Type = f.Key, Count = f.Count() }).OrderBy(a => a.Type);
                    foreach (var npctype in group)
                    {
                        TextArea txt = GUI.AddTextArea("npctxt", String.Format("{0}x", npctype.Count), new Rectangle(x, y, tileH, tileH), c);
                        txt.Font = Main.fontSmall;
                        x += tileH;
                        Sprite sprite = GUI.AddSprite("npcicon", new Rectangle(x, y, tileH, tileH), TileSet.Texture, c);
                        sprite.Source = TileSet.GetSourceRect(NpcType.AllAvailable[npctype.Type].gID);
                        x += tileH + 8;
                    }
                }
            }
            Control c2 = GUI.FindChild("thiswave", false);
            if (c2 != null)
            {
                c2.ClearChildren();
                int x = c2.Position.X + 16;
                int y = c2.Position.Y + 16;
                tileH = c2.Position.Height;
                var group = waveNpcs.GroupBy(n => n.Type.ID).Select(f => new { Type = f.Key, Count = f.Count() }).OrderBy(a => a.Type);
                foreach (var npctype in group)
                {
                    TextArea txt = GUI.AddTextArea("npctxt", String.Format("{0}x", npctype.Count), new Rectangle(x, y, tileH, tileH), c2);
                    txt.Font = Main.fontSmall;
                    x += tileH;
                    Sprite sprite = GUI.AddSprite("npcicon", new Rectangle(x, y, tileH, tileH), TileSet.Texture, c2);
                    sprite.Source = TileSet.GetSourceRect(NpcType.AllAvailable[npctype.Type].gID);
                    x += tileH + 8;
                }
            }

        }
        List<Npc> GetNPCsAtPosition(Vector2 gameWorldPos, float radius)
        {
            List<Npc> npcs = new List<Npc>();
            foreach (Npc npc in npcList)
            {
                if (Vector2.Distance(npc.TilePosition, gameWorldPos) <= radius)
                    npcs.Add(npc);
            }
            return npcs;
        }
        List<Npc> GenerateNpcWave(float mod)
        {
            if (currentWave == waveCount)
                return new List<Npc>();
            int count = random.Next((int)MathHelper.Lerp(1, 10, mod), (int)MathHelper.Lerp(5, 20, mod));
            List<Npc> list = new List<Npc>(count);
            float maxDelay = waveFreq / count;
            if (maxDelay > 2)
                maxDelay = 2;
            float minDelay = 0.01f;
            for (int i = 0; i < count; i++)
            {
                Npc npc = new Npc(NpcType.Random, currentWave + 1, this);
                npc.Path = GeneratePath();
                npc.TransAmt = MathHelper.Lerp(minDelay, maxDelay, (float)random.NextDouble()); // spawn delay
                list.Add(npc);
            }

            return list;
        }
        int[] GeneratePath()
        {
            List<int> path = new List<int>();
            return PathSection(startTile, endTile, ref path).ToArray();
        }
        List<int> PathSection(int start, int end, ref List<int> illegal)
        {
            List<int> path = new List<int>();
            int current = start;
            path.Add(current);
            while (current != end)
            {
                Direction d = PathConnection(current);

                if (d.HasFlag(Direction.Up) && (path.Contains(current - lvlWidth) || illegal.Contains(current - lvlWidth)))
                    d &= ~Direction.Up;
                if (d.HasFlag(Direction.Down) && (path.Contains(current + lvlWidth) || illegal.Contains(current + lvlWidth)))
                    d &= ~Direction.Down;
                if (d.HasFlag(Direction.Left) && (path.Contains(current - 1) || illegal.Contains(current - 1)))
                    d &= ~Direction.Left;
                if (d.HasFlag(Direction.Right) && (path.Contains(current + 1) || illegal.Contains(current + 1)))
                    d &= ~Direction.Right;
                if (d == Direction.None) // dead end 
                    return new List<int>();
                else // split in road
                {
                    var matching = Enum.GetValues(typeof(Direction)).Cast<Direction>().Where(c => c!= Direction.None && d.HasFlag(c)).ToList();
                    if (matching.Count == 1)
                    {
                        var dir = matching[0];
                        current += DirectionMod(matching[0]);
                        path.Add(current); // add new tile to list
                    }
                    else
                    {
                        while (matching.Count != 0)
                        {
                            var dir = matching[random.Next(0, matching.Count)]; // random direction after split
                            int newtile = current + DirectionMod(dir);
                            
                            List<int> mergedlist = path.Concat(illegal).ToList();
                            var subpath = PathSection(newtile, end, ref mergedlist); // get subpath of this new direction

                            if (subpath.Count != 0) // subpath leads to end
                            {
                                path.AddRange(subpath);
                                current = subpath[subpath.Count - 1];
                                break;
                            }
                            else // subpath ends in dead end
                                matching.Remove(dir);
                        }
                        if (matching.Count == 0) // all subpaths end ind dead end, meaning this path is not good.                       
                            return new List<int>();                        
                    }
                }
            }
            return path;
        }
        int DirectionMod(Direction d)
        {
            switch (d)
            {
                case Direction.Up:
                    return -lvlWidth;
                case Direction.Down:
                    return lvlWidth;
                case Direction.Left:
                    return -1;
                case Direction.Right:
                    return 1;
                default:
                    return 0;
            }
        }

        public int TileAtPosition(Vector2 position)
        {
            int x = (int)((position.X - paddingX) / tileW);
            int y = (int)((position.Y - paddingY - ControlBar.Position.Height)/ tileH);
            if (x < 0 || x >= lvlWidth || y < 0 || y >= lvlHeight)
                return -1;
            return x + y * lvlWidth;
        }
        /// <summary>
        /// Screen position at ground tile
        /// </summary>
        /// <param name="tile"></param>
        /// <returns></returns>
        public Vector2 PositionAtTile(int tile)
        {
            int x = tile % lvlWidth;
            int y = tile / lvlWidth;
            return new Vector2(x * tileW + paddingX + tileW / 2, y * tileH + paddingY + ControlBar.Position.Height + tileH / 2);
        }
        /// <summary>
        /// Converts screen position to gameworld position (1 unit = 1 tile)
        /// </summary>
        /// <param name="ScreenPosition"></param>
        /// <returns></returns>
        public Vector2 GameWorldPosition(Vector2 ScreenPosition)
        {
            return new Vector2((ScreenPosition.X - paddingX) / tileW, (ScreenPosition.Y - paddingY - ControlBar.Position.Height) / tileH);
        }
        public Vector2 ScreenPosition(Vector2 GamePosition)
        {
            return new Vector2(GamePosition.X * tileW + paddingX, GamePosition.Y * tileH + paddingY + TopBarHeight);
        }
        public Vector2 ScreenTileRatio(float tileRatio)
        {
            return new Vector2(tileRatio * tileW, tileRatio * tileH);
        }

        Rectangle RectAtTile(int tile)
        {
            return RectAtTile(tile % lvlWidth, tile / lvlWidth);
        }
        Rectangle RectAtTile(int x, int y)
        {
            return new Rectangle(x * tileW + paddingX, y * tileH + paddingY + ControlBar.Position.Height, tileW, tileH); 

        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, DepthStencilState.Default, RasterizerState.CullCounterClockwise);

            // background grass tile
            spriteBatch.Draw(TileSet.Texture, new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), TileSet.GetSourceRect(0), Color.White);

            // ground tiles
            for (int y = 0; y < lvlHeight; y++)
            {
                for (int x = 0; x < lvlWidth; x++)
                {
                    ushort tile = (ushort)(x + y * lvlWidth);
                    ushort gid = ground[tile];
                    Rectangle dest = RectAtTile(x, y);
                    spriteBatch.Draw(TileSet.Texture, dest, TileSet.GetSourceRect(gid), Color.White);

                    if (path[tile]) // draw road over ground tile
                    {
                        spriteBatch.Draw(TileSet.Texture, dest, TileSet.GetSourceRect(road[tile]), Color.White);
                    }    
                }
            }

            foreach (Npc npc in npcList) // draw NPCs
            {
                spriteBatch.Draw(TileSet.Texture, npc.Position, TileSet.GetSourceRect(npc.gID), Color.White, 0f, HalfOrigin, new Vector2(tileW / 24f, tileH / 24f), SpriteEffects.None, 0); // npc sprite
                spriteBatch.Draw(TileSet.Texture, npc.Position, TileSet.GetSourceRect(hpBarGID), Color.White, 0f, HalfOrigin, new Vector2((tileW / 24f) * npc.HP/npc.InitHP, tileH / 24f), SpriteEffects.None, 0); // health bar
            }

            if (selectedTower != null) // draw selection marker + tower's radius 
            {
                if (!placingTower) // at the selected tower's tile
                {
                    spriteBatch.Draw(TileSet.Texture, RectAtTile(selectedTower.Tile), TileSet.GetSourceRect(markerGID), Color.White);
                    spriteBatch.Draw(TileSet.Texture, PositionAtTile(selectedTower.Tile), TileSet.GetSourceRect(rangeGID), Color.White, 0f, HalfOrigin, new Vector2(selectedTower.Range * 2 * tileW / 16f, selectedTower.Range * 2 * tileH / 16f), SpriteEffects.None, 0);
                }
                else if (mouseTile != -1 && ground[mouseTile] == 1) // at the mouse tile if the tile is buildable           
                {
                    spriteBatch.Draw(TileSet.Texture, RectAtTile(mouseTile), TileSet.GetSourceRect(markerGID), Color.White);
                    spriteBatch.Draw(TileSet.Texture, PositionAtTile(mouseTile), TileSet.GetSourceRect(rangeGID), Color.White, 0f, HalfOrigin, new Vector2(selectedTower.Range * 2 * tileW / 16f, selectedTower.Range * 2 * tileH / 16f), SpriteEffects.None, 0);
                }
            }
            foreach (Tower t in towerList) // Draw TOWERS
            {
                // platform
                spriteBatch.Draw(TileSet.Texture, t.Position + (Vector2.UnitY * tileH / 8), TileSet.GetSourceRect(t.PlatformGID), Color.White, 0f, HalfOrigin, new Vector2((float)tileW / TileSet.TileW, (float)tileH / TileSet.TileH), SpriteEffects.None, 0); 
                // turret
                if (t.turret != null)
                    spriteBatch.Draw(TileSet.Texture, t.Position - (Vector2.UnitY * tileH / 8), TileSet.GetSourceRect(t.TurretGID), Color.White, t.Rotation, HalfOrigin, new Vector2((float)tileW / TileSet.TileW, (float)tileH / TileSet.TileH), SpriteEffects.None, 0);
                        
            }
            
            // Scenery, start end tile
            for (int y = 0; y < lvlHeight; y++)
            {
                for (int x = 0; x < lvlWidth; x++)
                {
                    ushort tile = (ushort)(x + y * lvlWidth);
                    Rectangle dest = RectAtTile(x, y);       
                    if (scenery[tile] != 0)
                    {
                        spriteBatch.Draw(TileSet.Texture, dest, TileSet.GetSourceRect(scenery[tile]), Color.White);
                    }
                    else if (tile == startTile)
                        spriteBatch.Draw(TileSet.Texture, dest, TileSet.GetSourceRect(startGID), Color.White);
                    else if (tile == endTile)
                    {
                        spriteBatch.Draw(TileSet.Texture, dest, TileSet.GetSourceRect(endGID), Color.White);
                        spriteBatch.Draw(TileSet.Texture, RectAtTile(x, y - 1), TileSet.GetSourceRect((ushort)(endGID + 1)), Color.White);
                    }
                }
            }

            //draw projectiles
            foreach (Projectile projectile in projectileList)
            {
                spriteBatch.Draw(TileSet.Texture, projectile.Position, TileSet.GetSourceRect(projectile.Specs.gID), Color.White, projectile.Rotation, HalfOrigin, new Vector2((float)tileW / TileSet.TileW, (float)tileH / TileSet.TileH), SpriteEffects.None, 0);
            }
            //draw animatedSprites
            foreach (AnimatedSprite sprite in animatedList)
            {
                spriteBatch.Draw(sprite.Sheet.Texture, sprite.Position, sprite.Sheet.GetSourceRect((ushort)sprite.Frame), Color.White, 0f, new Vector2(sprite.Sheet.TileW / 2, sprite.Sheet.TileH / 2), new Vector2((float)tileW / sprite.Sheet.TileW, (float)tileH / sprite.Sheet.TileH), SpriteEffects.None, 0);
            }


            // draw GUI
            GUI.Draw(spriteBatch);

            //draw animatedText
            foreach (TranText txt in animatedText)
            {
                spriteBatch.DrawString(Main.font, txt.Text, txt.Position, txt.Color);
            }

            // Draw mouse cursor
            if (placingTower && mouseTile != -1)  //as a selected tower if placing it            
            {
                if (selectedTower.platform != null)
                    spriteBatch.Draw(TileSet.Texture, new Rectangle(oldMouseState.X - tileW / 2, oldMouseState.Y - tileH / 2 + tileH / 4, tileW, tileH), TileSet.GetSourceRect(selectedTower.PlatformGID), Color.White);
                if (selectedTower.turret != null)
                    spriteBatch.Draw(TileSet.Texture, new Rectangle(oldMouseState.X - tileW / 2, oldMouseState.Y - tileH / 2, tileW, tileH), TileSet.GetSourceRect(selectedTower.TurretGID), Color.White);

            }
            else // as normal crosshair
                spriteBatch.Draw(crosshair, new Vector2(oldMouseState.X, oldMouseState.Y), null, Color.Gray, 0f, new Vector2(32, 32), 1.2f, SpriteEffects.None, 0);     

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
