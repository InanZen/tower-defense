using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XNA_GUI_Controls;

namespace TowerDefense
{
    [Flags]
    enum Direction
    {
        None = 0,
        Up = 1,
        Down = 2,
        Left = 4,
        Right = 8
    }

    enum GUI_Frame_Style
    {
        Default = 0
    }
    enum GUI_Button_Style
    {
        Default = 0,
        Pause = 1,
        Play = 2,
        Slider = 3,
        Checkbox = 4,
        Left = 5,
        Right = 6,
        Menu = 7
    }
    enum GUI_Background_Style
    {
        Slider = 0
    }

    [Serializable]
    public class GameSettings
    {
        public bool PreserveRatio = true;
        public float MusicVolume = 1f;
        public float SFXVolume = 1f;
        public bool Fullscreen = false;
    }
    public class PopUp
    {
        public Control Host;
        public FrameBox Frame;
        public bool KeepMouseDistance;
        public Point MouseDistance = new Point(32, 32);
        public TextArea textArea;
        public string Text
        {
            get { return textArea.Text; }
            set { textArea.Text = value; }
        }
        public Rectangle Position
        {
            get { return Frame.Position; }
            set
            {
                Frame.Position = value;
            }
        }
        public void CloseEvent(object sender, EventArgs args)
        {
            if (Frame != null)
            {
                Frame.Parent = null;
                Frame.ClearChildren();
            }
            Host.OnMouseLeave -= CloseEvent;
        }
        public void Update()
        {
            if (KeepMouseDistance)
            {
                MouseState ms = Mouse.GetState();
                Position = new Rectangle(ms.X + MouseDistance.X, ms.Y + MouseDistance.Y, Position.Width, Position.Height);
            }
        }
        public PopUp(Control host, Rectangle pos,  int frameStyle, string text)
        {
            Host = host;
            Controller c = host.TopParent as Controller;
            Frame = c.AddFrame("popup_frame", pos, frameStyle);
            textArea = c.AddTextArea("popup_text", text, Frame.InnerRect, Frame);
            host.OnMouseLeave += CloseEvent;
        }
    }
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Main : Microsoft.Xna.Framework.Game
    {

        GraphicsDeviceManager graphics;
        GraphicsDevice device;
        SpriteBatch spriteBatch;

        public static SpriteFont font;
        public static SpriteFont fontSmall;
        public static SpriteFont fontNumbers;
        public static SpriteFont fontFipps;
        public static AudioComponent Audio;
        public static GameSettings Settings;
        public static PopUp popUp;
        public Controller GUI;
        public static String AppDir;

        public Main()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            //IsMouseVisible = true;
            graphics.PreferredBackBufferWidth = 1024;
            graphics.PreferredBackBufferHeight = 768;

            Window.AllowUserResizing = true;
            Window.ClientSizeChanged += Window_ClientSizeChanged;

        }
        void Window_ClientSizeChanged(object sender, EventArgs e)
        {
            GUI.Position = new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);
        }


        protected override void Initialize()
        {
            device = GraphicsDevice;
            Components.Add(new FrameRateCounter(this));
            AppDir  = Path.Combine(Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)).FullName, "Local", "Inan Defense");
            if (!Directory.Exists(AppDir))
            {
                try
                {
                    Directory.CreateDirectory(AppDir);
                }
                catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            }
            base.Initialize();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                SaveSettings();
            }
            base.Dispose(disposing);
        }

        void SaveSettings()
        {
            try
            {
                string settingsPath = Path.Combine(AppDir, "settings.dat");
                using (FileStream fs = new FileStream(settingsPath, FileMode.Create, FileAccess.Write))
                {
                    BinaryFormatter bf = new BinaryFormatter();
                    bf.Serialize(fs, Main.Settings);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        protected override void LoadContent()
        {
            Settings = new GameSettings();
            try
            {
                string settingsPath = Path.Combine(AppDir, "settings.dat");
                if (File.Exists(settingsPath))
                {
                    using (FileStream fs = new FileStream(settingsPath, FileMode.Open, FileAccess.Read))
                    {
                        BinaryFormatter bf = new BinaryFormatter();
                        Settings = (GameSettings)bf.Deserialize(fs);
                    }
                    MediaPlayer.Volume = Settings.MusicVolume;
                    SoundEffect.MasterVolume = Settings.SFXVolume;
                    if (Settings.Fullscreen)
                    {
                        graphics.IsFullScreen = true;
                        graphics.PreferredBackBufferWidth = GraphicsDevice.Adapter.CurrentDisplayMode.Width;
                        graphics.PreferredBackBufferHeight = GraphicsDevice.Adapter.CurrentDisplayMode.Height;
                        graphics.ApplyChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            spriteBatch = new SpriteBatch(device);
            font = Content.Load<SpriteFont>("Font");
            fontSmall = Content.Load<SpriteFont>("FontSmall");
            fontNumbers = Content.Load<SpriteFont>("FontNumbers");
            fontFipps = Content.Load<SpriteFont>("FippsFont");             

            GUI = new Controller("controller", new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), Main.font);

            GUI.AddFrameStyle(Content.Load<Texture2D>(@"gui/frame_orange"), 16, 16);

            GUI.AddButtonStyle(Content.Load<Texture2D>(@"gui/button_orange"), 55, 18);
            GUI.AddButtonStyle(Content.Load<Texture2D>(@"gui/pause"), 16, 16);
            GUI.AddButtonStyle(Content.Load<Texture2D>(@"gui/play"), 16, 16);
            GUI.AddButtonStyle(Content.Load<Texture2D>(@"gui/slider_handle"), 5, 10);
            GUI.AddButtonStyle(Content.Load<Texture2D>(@"gui/checkbox"), 32, 32);
            GUI.AddButtonStyle(Content.Load<Texture2D>(@"gui/left"), 32, 32);
            GUI.AddButtonStyle(Content.Load<Texture2D>(@"gui/right"), 32, 32);
            GUI.AddButtonStyle(Content.Load<Texture2D>(@"gui/menu"), 32, 32);

            GUI.AddBackgroundStyle(Content.Load<Texture2D>(@"gui/slider_bg"));

            Audio = new AudioComponent(this);
            Components.Add(Audio);

            TitleScreen title = new TitleScreen(this);
            title.Initialize();
            Components.Add(title);

        }
        public void ShowSettings()
        {
            int w2 = GraphicsDevice.Viewport.Width / 2;
            int h2 = GraphicsDevice.Viewport.Height / 2;

            var frame = GUI.AddFrame("settings", new Rectangle(w2 - 200, h2 - 150, 400, 300), 0);
            frame.BorderH = 6;
            frame.BorderW = 6;
            Rectangle inner = frame.InnerRect;

            int elementPadding = 24;
            int curY = inner.Y + (inner.Height / 8);
            w2 = inner.Width / 2;

            var text = GUI.AddTextArea("fs_text", "Fullscreen:", new Rectangle(inner.X, curY, w2, 32), frame);
            text.Alignment = TextAlignment.Right;
            text.OnMouseOver += OnMouseOver;
            var button = GUI.AddToggleButton("fs_button", null, new Rectangle(inner.Center.X + elementPadding, curY, 32, 32), OnButtonClick, (int)GUI_Button_Style.Checkbox, frame);
            if (Main.Settings.Fullscreen)
                button.State = 1;
            curY += 32 + elementPadding;

            text = GUI.AddTextArea("ratio_text", "Preserve tile ratio:", new Rectangle(inner.X, curY, w2, 32), frame);
            text.Alignment = TextAlignment.Right;
            text.OnMouseOver += OnMouseOver;
            button = GUI.AddToggleButton("ratio_button", null, new Rectangle(inner.Center.X + elementPadding, curY, 32, 32), OnButtonClick, (int)GUI_Button_Style.Checkbox, frame);
            button.OnMouseOver += OnMouseOver;
            if (Main.Settings.PreserveRatio)
                button.State = 1;
            curY += 32 + elementPadding;


            text = GUI.AddTextArea("music_text", "Music volume:", new Rectangle(inner.X, curY, w2, 32), frame);
            text.Alignment = TextAlignment.Right;
            var slider = GUI.AddSlider("music_val", new Rectangle(inner.Center.X + elementPadding, curY, w2 - elementPadding * 2, 32), (int)GUI_Background_Style.Slider, (int)GUI_Button_Style.Slider, frame);
            slider.Value = MediaPlayer.Volume;
            slider.ValueChanged += OnSliderChange;
            curY += 32 + elementPadding;

            text = GUI.AddTextArea("sfx_text", "Sound effects volume:", new Rectangle(inner.X, curY, w2, 32), frame);
            text.Alignment = TextAlignment.Right;
            slider = GUI.AddSlider("sfx_val", new Rectangle(inner.Center.X + elementPadding, curY, w2 - elementPadding * 2, 32), (int)GUI_Background_Style.Slider, (int)GUI_Button_Style.Slider, frame);
            slider.Value = SoundEffect.MasterVolume;
            slider.ValueChanged += OnSliderChange;

            var b = GUI.AddButton("settings_close", "Done", new Rectangle(inner.Center.X - 50, inner.Bottom - 40, 100, 36), OnButtonClick, (int)GUI_Button_Style.Default, frame);
        }
        void OnMouseOver(object sender, EventArgs args)
        {
            Control c = sender as Control;
            if (c.Name == "ratio_text" || c.Name == "ratio_button")
            {
                MouseState ms = Mouse.GetState();
                popUp = new PopUp(c, new Rectangle(ms.X - 128, ms.Y - 160, 256, 128), (int)GUI_Frame_Style.Default, "Keeps graphics in 1:1 ratio when you resize the window");
                popUp.KeepMouseDistance = true;
                popUp.MouseDistance = new Point(-128, -160);
            }
        }
        void OnSliderChange(object sender, EventArgs args)
        {
            Slider s = sender as Slider;
            if (s.Name == "music_val")
            {
                MediaPlayer.Volume = s.Value;
                Settings.MusicVolume = s.Value;
            }
            else if (s.Name == "sfx_val")
            {
                SoundEffect.MasterVolume = s.Value;
                Settings.SFXVolume = s.Value;
            }
        }
        void OnButtonClick(Object sender, EventArgs args)
        {
            Control c = sender as Control;
            if (c.Name == "ratio_button")
            {
                ToggleButton b = sender as ToggleButton;
                if (b.State == 0 || b.State == 3)
                    Settings.PreserveRatio = true;
                else
                    Settings.PreserveRatio = false;
            }
            else if (c.Name == "fs_button")
            {
                ToggleButton b = sender as ToggleButton;
                if (b.State == 0 || b.State == 3)
                {
                    Settings.Fullscreen = true;
                    graphics.IsFullScreen = true;
                    graphics.PreferredBackBufferWidth = GraphicsDevice.Adapter.CurrentDisplayMode.Width;
                    graphics.PreferredBackBufferHeight = GraphicsDevice.Adapter.CurrentDisplayMode.Height;
                }
                else
                {
                    Settings.Fullscreen = false;
                    graphics.IsFullScreen = false;
                }
                graphics.ApplyChanges();
            }
            else if (c.Name == "settings_close")
                GUI.Remove(c.Parent);
        }


        protected override void Update(GameTime gameTime)
        {
            if (popUp != null)
                popUp.Update();
            GUI.Update(gameTime);
            base.Update(gameTime);
        }
        protected override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
