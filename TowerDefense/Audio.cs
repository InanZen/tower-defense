using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace TowerDefense
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class AudioComponent : Microsoft.Xna.Framework.GameComponent
    {

        Dictionary<String, SoundEffect> Sounds = new Dictionary<string, SoundEffect>();
        Song[] SongList;
        int index = 0;
        byte status;
        Random rand = new Random();
        /// <summary>
        /// 0 = intro, 1 = game, 2 = paused
        /// </summary>
        public byte Status
        {
            get { return status; }
            set
            {
                if (value == 1)                
                    index = rand.Next(1, SongList.Length); // choose random song when switching to game   
                status = value;
                MediaPlayer.Stop(); // stop current track
            }
        }

    
        public bool PlaySound(string name)
        {
            if (Sounds.ContainsKey(name))
            {
                Sounds[name].Play();
                return true;
            }
            else
                return false;
        }


        public AudioComponent(Game game)
            : base(game)
        {
            Sounds.Add("explosion", game.Content.Load<SoundEffect>(@"sounds/explosion"));
            Sounds.Add("hit_light", game.Content.Load<SoundEffect>(@"sounds/hit_light"));
            Sounds.Add("fire", game.Content.Load<SoundEffect>(@"sounds/fire"));
            Sounds.Add("spit", game.Content.Load<SoundEffect>(@"sounds/spit"));
            Sounds.Add("gun", game.Content.Load<SoundEffect>(@"sounds/gun"));
            Sounds.Add("gun2", game.Content.Load<SoundEffect>(@"sounds/gun2"));
            Sounds.Add("airgun", game.Content.Load<SoundEffect>(@"sounds/airgun"));
            Sounds.Add("error", game.Content.Load<SoundEffect>(@"sounds/error"));
            Sounds.Add("sell", game.Content.Load<SoundEffect>(@"sounds/sell"));
            Sounds.Add("upgrade", game.Content.Load<SoundEffect>(@"sounds/upgrade"));
            Sounds.Add("click", game.Content.Load<SoundEffect>(@"sounds/click"));
            Sounds.Add("destroyed", game.Content.Load<SoundEffect>(@"sounds/destroyed"));
            Sounds.Add("win", game.Content.Load<SoundEffect>(@"sounds/win"));
            Sounds.Add("scream", game.Content.Load<SoundEffect>(@"sounds/scream"));

            SongList = new Song[3];
            SongList[0] = Game.Content.Load<Song>(@"music/intro");
            SongList[1] = Game.Content.Load<Song>(@"music/Theme of Agrual");
            SongList[2] = Game.Content.Load<Song>(@"music/Evasion");
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {

            base.Initialize();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                MediaPlayer.Stop();
                foreach (SoundEffect sound in Sounds.Values)
                {
                    sound.Dispose();
                } 
                foreach (Song song in SongList)
                {
                    song.Dispose();
                }
            }
            
            base.Dispose(disposing);
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            if (Game.IsActive)
            {
                if (MediaPlayer.State == MediaState.Paused)
                    MediaPlayer.Resume();
                else if (MediaPlayer.State == MediaState.Stopped)
                {
                    if (Status == 0) // title
                        MediaPlayer.Play(SongList[0]);
                    else
                    {
                        index++;
                        if (index >= SongList.Length)
                            index = 1;
                        MediaPlayer.Play(SongList[index]);
                    }
                }
            }
            else if (MediaPlayer.State == MediaState.Playing)
                MediaPlayer.Pause();

            base.Update(gameTime);
        }
    }
}
