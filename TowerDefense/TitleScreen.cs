using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;
using XNA_GUI_Controls;

namespace TowerDefense
{
    public struct LevelData
    {
        public int Money;
        public int HP;
        public float WaveTime;
        public int WaveCount;
        public string ImgPath;

    }
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class TitleScreen : Microsoft.Xna.Framework.DrawableGameComponent
    {
        Controller GUI;
        Texture2D Background;
        Texture2D Cursor;
        SpriteBatch spriteBatch;
        Vector2 mousePos;
        Vector2 cursorOrigin;
        TitlePage displaying = TitlePage.Title;
        TitlePage Displaying
        {
            get
            {
                return displaying;
            }
            set
            {
                if (displaying != value)
                {
                    displaying = value;
                    GUI.ClearChildren();
                    ShowEssentials();
                    if (displaying == TitlePage.LvlSel)
                        ShowLvlSelect();
                    else if (displaying == TitlePage.Options)
                    {
                        Main main = Game as Main;
                        main.ShowSettings();
                        Control b = GUI.FindChild("settings_close", true);
                        if (b != null)
                            b.OnClick += OnButtonClick;
                    }
                    else
                        ShowTitle();
                }
            }
        }
        MouseState oldMouseState;
        KeyboardState oldKeyState;

        enum TitlePage
        {
            Title = 0,
            LvlSel = 1,
            Options = 2
        }

        
        public TitleScreen(Game game)
            : base(game)
        {
            Main main = game as Main;
            GUI = main.GUI;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            Main.Audio.Status = 0;
            oldMouseState = Mouse.GetState();
            oldKeyState = Keyboard.GetState();
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Background = Game.Content.Load<Texture2D>("background");
            Cursor = Game.Content.Load<Texture2D>(@"gui/crosshair6");
            cursorOrigin = new Vector2(Cursor.Width / 2, Cursor.Height / 2);
            ShowEssentials();
            ShowTitle();
            base.LoadContent();
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
        void ShowEssentials()
        {
            int w2 = GraphicsDevice.Viewport.Width / 2;
            TextArea title = GUI.AddTextArea("title", "Inan Defense", new Rectangle(0, 0, w2 * 2, 64));
            title.Font = Main.fontFipps;
            title.Color = Color.Wheat;
            TextArea versionTxt = GUI.AddTextArea("version", "v 1.1", new Rectangle(w2 + w2 / 2, GraphicsDevice.Viewport.Height - 64, w2 / 2, 64));
            versionTxt.Color = Color.WhiteSmoke;
        }
        void ShowTitle()
        {
            int w2 = GraphicsDevice.Viewport.Width / 2;
            int h2 = GraphicsDevice.Viewport.Height / 2;
            GUI.AddButton("levelselect", "Level Select", new Rectangle(w2 - 100, h2 - 64 * 2 - 8 * 2, 200, 64), OnButtonClick, (int)GUI_Button_Style.Default);
            GUI.AddButton("leveleditor", " ", new Rectangle(w2 - 100, h2 - 64 - 8, 200, 64), OnButtonClick, (int)GUI_Button_Style.Default);
            GUI.AddButton("settings", "Settings", new Rectangle(w2 - 100, h2, 200, 64), OnButtonClick, (int)GUI_Button_Style.Default);
            GUI.AddButton("exit", "Exit", new Rectangle(w2 - 100, h2 + 64 + 8, 200, 64), OnButtonClick, (int)GUI_Button_Style.Default);

        }
        void ShowLvlSelect(int page = 0)
        {
            int numperrow = 3;
            int rows = 3;
            int pagemod = page * rows * numperrow;
            int wDif = GraphicsDevice.Viewport.Width / (numperrow + 1);
            int hDif = (GraphicsDevice.Viewport.Height - 64) / (rows + 1);
            int w = wDif - 48;
            int h = hDif - 48;
            string path = Path.Combine(Main.AppDir, "levels");
            String[] files = new String[0];
            try
            {
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                files = Directory.GetFiles(path, "*.xml");
            }
            catch (UnauthorizedAccessException ex)
            {
                var frame = GUI.AddFrame("errorframe", new Rectangle(64, 100, GraphicsDevice.Viewport.Width - 128, GraphicsDevice.Viewport.Height - 210), (int)GUI_Frame_Style.Default);
                GUI.AddTextArea("failtxt", String.Format("Can't read the folder: \"{0}\". Make sure you have the required privileges or try running the game in administrator mode.", path), frame.InnerRect, frame);
                GUI.AddButton("openlvlfolder", "Open level folder", new Rectangle(frame.InnerRect.Center.X - 128, frame.InnerRect.Bottom - 48, 256, 48), OnButtonClick, (int)GUI_Button_Style.Default, frame);
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            for (int r = 0; r < rows; r++)
            {
                for (int x = 0; x < numperrow; x++)
                {
                    int id = pagemod + x + r * numperrow;
                    if (id >= files.Length)
                        break;
                    XmlDocument xmlDoc = new XmlDocument(); //* create an xml document object.
                    xmlDoc.Load(files[id]); //* load the XML document from the specified file.

                    string name = xmlDoc.GetElementsByTagName("name")[0].InnerText;
                    string imgFile = xmlDoc.GetElementsByTagName("img")[0].InnerText;
                    int waveCount;
                    int.TryParse(xmlDoc.GetElementsByTagName("waveCount")[0].InnerText, out waveCount);
                    float waveTime;
                    float.TryParse(xmlDoc.GetElementsByTagName("waveTime")[0].InnerText, out waveTime);
                    int money;
                    int.TryParse(xmlDoc.GetElementsByTagName("money")[0].InnerText, out money);
                    int hp;
                    int.TryParse(xmlDoc.GetElementsByTagName("HP")[0].InnerText, out hp);

                    LevelData lData = new LevelData { ImgPath = imgFile, Money = money, WaveTime = waveTime, WaveCount = waveCount, HP = hp };
                    Texture2D texture = null;
                    try
                    {
                        using (FileStream fs = new FileStream(Path.Combine(path, imgFile), FileMode.Open))
                        {
                            texture = Texture2D.FromStream(GraphicsDevice, fs);
                        }
                    }
                    catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                    if (texture == null)
                        continue;

                    Rectangle pos = new Rectangle(wDif * (x + 1) - w / 2, 64 + hDif * (r + 1) - h / 2, w, h);
                    TextArea txt = GUI.AddTextArea("lvl_name", name, new Rectangle(pos.X, pos.Y - 32, pos.Width, 12));
                    txt.Color = Color.White;
                    Sprite s = GUI.AddSprite("lvlicon", pos, texture);
                    s.Tag = lData;
                    s.OnClick += OnButtonClick;
                    s.OnMouseOver += OnMouseOver;

                }
            }

            var button = GUI.AddButton("title", null, new Rectangle(GraphicsDevice.Viewport.Width / 2 - 32, GraphicsDevice.Viewport.Height - 96, 64, 64), OnButtonClick, (int)GUI_Button_Style.Menu);
          
            button = GUI.AddButton("lvlpage", null, new Rectangle(GraphicsDevice.Viewport.Width / 2 - 128, GraphicsDevice.Viewport.Height - 96, 64, 64), OnButtonClick, (int)GUI_Button_Style.Left);
            button.Tag = page - 1;
            if (pagemod == 0)
                button.Disabled = true;
            button = GUI.AddButton("lvlpage", null, new Rectangle(GraphicsDevice.Viewport.Width / 2 + 64, GraphicsDevice.Viewport.Height - 96, 64, 64), OnButtonClick, (int)GUI_Button_Style.Right);
            button.Tag = page + 1;
            if (files.Length < rows * numperrow * (page + 1))
                button.Disabled = true;

        }

        void HighlightEvent(object sender, EventArgs args)
        {
            Control c = sender as Control;
            c.Position = new Rectangle(c.Position.X - 8, c.Position.Y - 8, c.Position.Width + 16, c.Position.Height + 16);
        }
        void EndHighlightEvent(object sender, EventArgs args)
        {
            Control c = sender as Control;
            c.Position = new Rectangle(c.Position.X + 8, c.Position.Y + 8, c.Position.Width - 16, c.Position.Height - 16);
        }
        
        void OnMouseOver(object sender, EventArgs args)
        {
            Control c = sender as Control;
            if (c.Name == "lvlicon")
            {
                LevelData data = (LevelData)c.Tag;
                Main.popUp = new PopUp(c, new Rectangle(oldMouseState.X + 24, oldMouseState.Y - 50, 200, 128), (int)GUI_Frame_Style.Default, String.Format("Money: {0}    HP: {2}      Waves: {1}",data.Money, data.WaveCount, data.HP));
                Main.popUp.KeepMouseDistance = true;
                Main.popUp.MouseDistance = new Point(24, -50);
            }
        }

        protected override void UnloadContent()
        {
            base.UnloadContent();
        }
        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            KeyboardState keyState = Keyboard.GetState();
            if (keyState.IsKeyDown(Keys.Escape) && oldKeyState.IsKeyUp(Keys.Escape))
            {
                if (Displaying == TitlePage.Title)
                    Game.Exit();
                else
                    Displaying = TitlePage.Title;
                

            }
            MouseState mouseState = Mouse.GetState();
            mousePos = new Vector2(mouseState.X, mouseState.Y);
            
            oldMouseState = mouseState;
            oldKeyState = keyState;

            base.Update(gameTime);
        }
        void OnButtonClick(Object sender, EventArgs args)
        {
            Control c = sender as Control;
            Main.Audio.PlaySound("click");
            switch (c.Name)
            {
                case "levelselect":
                    {
                        Displaying = TitlePage.LvlSel;
                        break;
                    }
                case "lvlpage":
                    {
                        int page = (int)c.Tag;
                        GUI.ClearChildren();
                        ShowEssentials();
                        ShowLvlSelect(page);
                        break;
                    }
                case "settings":
                    {
                        Displaying = TitlePage.Options;
                        break;
                    }
                case "title":
                case "settings_close":
                    {
                        Displaying = TitlePage.Title;
                        break;
                    }
                case "openlvlfolder":
                    {
                        System.Diagnostics.Process.Start("explorer.exe", Path.Combine(Main.AppDir, "levels"));
                        break;
                    }         
                case "exit":
                    {
                        Game.Exit();
                        break;
                    }
                case "lvlicon":
                    {
                        try
                        {
                            LevelData lData = (LevelData)c.Tag;
                            GameComponent gComp = new GameComponent(Game);
                            Game.Components.Add(gComp);
                            gComp.LoadLevel(lData);
                            Game.Components.Remove(this);
                            this.Dispose(true);
                        }
                        catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                        break;
                    }
            }
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, DepthStencilState.Default, RasterizerState.CullCounterClockwise);
            spriteBatch.Draw(Background, new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), Color.White); // background
            GUI.Draw(spriteBatch);
            spriteBatch.Draw(Cursor, mousePos, null, Color.White, 0f, cursorOrigin, 1f, SpriteEffects.None, 0);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
